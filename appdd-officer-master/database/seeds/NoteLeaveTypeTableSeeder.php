<?php

use Illuminate\Database\Seeder;
use App\Models\NoteLeaveType;

class NoteLeaveTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NoteLeaveType::truncate();

        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ลาป่วย'],
            ['nl_type_level' => 1],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ลากิจ'],
            ['nl_type_level' => 1],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ลาพักร้อน'],
            ['nl_type_level' => 2],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ON-SITE'],
            ['nl_type_level' => 1],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ขาดงาน'],
            ['nl_type_level' => 2],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'ลากิจกรรม'],
            ['nl_type_level' => 1],
            ['nl_type_status' => 1]
        );
        NoteLeaveType::firstOrCreate(
            ['nl_type_name' => 'Remote Work'],
            ['nl_type_level' => 1],
            ['nl_type_status' => 1]
        );
    }
}
