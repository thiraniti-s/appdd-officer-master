<?php

use Illuminate\Database\Seeder;
use App\Models\OfficerType;

class OfficerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OfficerType::truncate();

        OfficerType::firstOrCreate(
            ['off_type_name' => 'พนักงานประจำ-รายเดือน'],
            ['off_type_status' => 1]
        );
    }
}
