<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOffGithubUserToTbOfficer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_officer', function (Blueprint $table) {
            $table->string('off_github_user')->nullable()->comment('Github username')->after('off_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_officer', function (Blueprint $table) {
            $table->dropColumn('off_github_user');
        });
    }
}
