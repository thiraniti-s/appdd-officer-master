<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_officer', function (Blueprint $table) {
            $table->increments('off_id')->comment('ไอดี');
            $table->string('off_code', 25)->nullable()->comment('ไอดีพนักงาน');
            $table->integer('off_type_id')->unsigned()->comment('รหัสประเภทพนักงาน');
            $table->string('off_prefixname', 25)->comment('คำนำหน้าชื่อ');
            $table->string('off_firstname', 100)->comment('ชื่อ');
            $table->string('off_lastname', 100)->comment('สกุล');
            $table->string('off_nickname', 50)->nullable()->comment('ชื่อเล่น');
            $table->string('off_image', 255)->nullable()->comment('รูปภาพ');
            $table->string('off_personal_id', 25)->nullable()->comment('เลขประจำตัวประชาชน');
            $table->date('off_bdate')->nullable()->comment('วันเกิด');
            $table->string('off_tel', 25)->comment('เบอร์โทร, มือถือ');
            $table->string('off_email', 100)->nullable()->comment('อีเมล์');
            $table->date('off_start_working')->nullable()->comment('วันที่เริ่มงาน');
            $table->date('off_end_working')->nullable()->comment('วันที่สิ้นสุดงาน');
            $table->string('off_position', 50)->nullable()->comment('ตำแหน่ง');
            $table->string('off_bank_acc_name', 50)->nullable()->comment('ชื่อบัญชีธนาคาร');
            $table->string('off_bank_acc_number', 50)->nullable()->comment('เลขที่บัญชีธนาคาร');
            $table->string('off_username', 50)->nullable()->comment('ชื่อเข้าใช้งาน');
            $table->string('off_password', 150)->nullable()->comment('รหัสผ่าน');
            $table->tinyInteger('off_permission_level')->default(0)->comment('ระดับสิทธิ์ [0=พนักงาน, 1=แอดมิน]');
            $table->tinyInteger('off_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ไม่ใช้งาน]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');

            // set foreign key
            $table->foreign('off_type_id')->references('off_type_id')->on('tb_officer_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_officer');
    }
}
