<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectOfficerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_project_officer', function (Blueprint $table) {
            $table->increments('prof_id')->comment('ไอดี');
            $table->integer('pro_id')->unsigned()->nullable()->comment('ไอดีโปรเจค');
            $table->integer('off_id')->unsigned()->nullable()->comment('ไอดีพนักงาน');
            $table->string('prof_skill', 255)->nullable()->comment('เฟรมเวิคที่ใช้, ภาษาที่ใช้');
            $table->string('prof_tools', 255)->nullable()->comment('เครื่องมือที่ใช้พัฒนา');
            $table->tinyInteger('prof_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ยกเลิก]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');

            // set foreign key
            $table->foreign('pro_id')->references('pro_id')->on('tb_project')->onDelete('set null');
            $table->foreign('off_id')->references('off_id')->on('tb_officer')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_project_officer');
    }
}
