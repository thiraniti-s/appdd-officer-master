<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_note_leave', function (Blueprint $table) {
            $table->increments('nl_id')->comment('ไอดี');
            $table->integer('off_id')->unsigned()->comment('ไอดีพนักงาน');
            $table->integer('nl_type_id')->unsigned()->comment('ประเภทการลางาน');
            $table->tinyInteger('nl_time_type')->default(1)->comment('ประเภทเวลาการลางาน [1=ลาเต็มวัน, 2=ลาบางชั่วโมง]');
            $table->string('nl_note', 255)->comment('บันทึกการลา / อาการป่วย / หมายเหตุ');
            $table->string('nl_file', 255)->nullable()->comment('เอกสารแนบ');
            $table->date('nl_leave_start')->comment('วันที่เริ่มลา');
            $table->date('nl_leave_end')->comment('วันที่สิ้นสุดลา');
            $table->time('nl_time_start')->nullable()->comment('เวลาที่เริ่มลา');
            $table->time('nl_time_end')->nullable()->comment('เวลาที่สิ้นสุดลา');
            $table->tinyInteger('nl_status')->default(1)->comment('สถานะ [1=ยื่นใบลา, 2=รอตรวจสอบ, 3=อนุมัติ, 0=ยกเลิกการยื่น, -1=ไม่อนุมัติ, -2=ยกเลิกโดยผู้มีสิทธิ์สูง]');
            $table->integer('nl_review_by')->unsigned()->nullable()->comment('ตรวจสอบโดย');
            $table->integer('nl_approve_by')->unsigned()->nullable()->comment('อนุมัติโดย');
            $table->integer('nl_reject_by')->unsigned()->nullable()->comment('ปฏิเสธโดย');
            $table->string('nl_reject_comment', 255)->nullable()->comment('หมายเหตุการปฏิเสธ');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');

            // set foreign key
            $table->foreign('off_id')->references('off_id')->on('tb_officer');
            $table->foreign('nl_type_id')->references('nl_type_id')->on('tb_note_leave_type');
            $table->foreign('nl_review_by')->references('off_id')->on('tb_officer');
            $table->foreign('nl_approve_by')->references('off_id')->on('tb_officer');
            $table->foreign('nl_reject_by')->references('off_id')->on('tb_officer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_note_leave');
    }
}
