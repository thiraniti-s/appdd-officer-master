<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnounceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_announce', function (Blueprint $table) {
            $table->increments('ann_id')->comment('ไอดี');
            $table->integer('off_id')->unsigned()->nullable()->comment('ผู้ประกาศ');
            $table->text('ann_text')->comment('ข้อความประกาศ');
            $table->date('ann_date')->comment('วันที่ประกาศ');
            $table->tinyInteger('ann_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ยกเลิก]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');

            // set foreign key
            $table->foreign('off_id')->references('off_id')->on('tb_officer')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_announces');
    }
}
