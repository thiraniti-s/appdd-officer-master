<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNlTypeLevelToTbNoteLeaveTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_note_leave_type', function (Blueprint $table) {
            $table->tinyInteger('nl_type_level')->default(1)->comment('ระดับการเข้าถึงประเภท')->after('nl_type_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_note_leave_type', function (Blueprint $table) {
            $table->dropColumn('nl_type_level');
        });
    }
}
