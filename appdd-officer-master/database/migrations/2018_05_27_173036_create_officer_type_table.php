<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficerTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_officer_type', function (Blueprint $table) {
            $table->increments('off_type_id')->comment('ไอดี');
            $table->string('off_type_name')->comment('ชื่อประเภทพนักงาน');
            $table->tinyInteger('off_type_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ไม่ใช้งาน]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_officer_type');
    }
}
