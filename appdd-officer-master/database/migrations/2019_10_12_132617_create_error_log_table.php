<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_error_log', function (Blueprint $table) {
            $table->increments('err_id')->comment('ไอดี');
            $table->string('err_source', 100)->comment('source');
            $table->text('err_exception')->comment('exception');
            $table->tinyInteger('err_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ยกเลิก]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_error_log');
    }
}
