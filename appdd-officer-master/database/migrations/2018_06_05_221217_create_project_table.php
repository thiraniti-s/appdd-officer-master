<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_project', function (Blueprint $table) {
            $table->increments('pro_id')->comment('ไอดี');
            $table->string('pro_name', 255)->comment('ชื่อโปรเจค');
            $table->text('pro_description')->comment('คำอธิบาย');
            $table->string('pro_application', 100)->nullable()->comment('ประเภทแอพพลิเคชั่น');
            $table->string('pro_skill', 255)->nullable()->comment('เฟรมเวิคที่ใช้, ภาษาที่ใช้');
            $table->string('pro_tools', 255)->nullable()->comment('เครื่องมือที่ใช้พัฒนา');
            $table->date('pro_start')->nullable()->comment('เริ่มโปรเจคเมื่อ');
            $table->date('pro_end')->nullable()->comment('จบโปรเจคเมื่อ');
            $table->tinyInteger('pro_status')->default(1)->comment('สถานะ [1=กำลังดำเนินการ, 2=เสร็จสิ้น (Maintenance), 3=ปิดโปรเจค (Finished), 0=ยกเลิก]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_project');
    }
}
