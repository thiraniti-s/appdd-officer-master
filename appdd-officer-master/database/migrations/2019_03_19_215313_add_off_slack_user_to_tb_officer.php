<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOffSlackUserToTbOfficer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_officer', function (Blueprint $table) {
            $table->string('off_slack_user')->nullable()->comment('Slack username')->after('off_github_user');
            $table->string('off_slack_webhook_token')->nullable()->comment('Slack webhook token')->after('off_slack_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_officer', function (Blueprint $table) {
            $table->dropColumn('off_slack_user');
            $table->dropColumn('off_slack_webhook_token');
        });
    }
}
