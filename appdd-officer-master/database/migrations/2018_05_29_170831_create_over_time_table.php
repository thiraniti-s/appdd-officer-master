<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_over_time', function (Blueprint $table) {
            $table->increments('ot_id')->comment('ไอดี');
            $table->integer('off_id')->unsigned()->comment('ไอดีพนักงาน');
            $table->string('ot_note', 255)->comment('บันทึกการทำงานล่วงเวลา / หมายเหตุ');
            $table->string('ot_reference', 100)->comment('บุคคลอ้างอิง');
            $table->string('ot_file', 255)->nullable()->comment('เอกสารแนบ');
            $table->date('ot_date')->comment('วันที่ทำงานล่วงเวลา');
            $table->time('ot_time_start')->nullable()->comment('เวลาที่เริ่ม');
            $table->time('ot_time_end')->nullable()->comment('เวลาที่สิ้นสุด');
            $table->tinyInteger('ot_status')->default(1)->comment('สถานะ [1=ยื่นใบล่วงเวลา, 2=รอตรวจสอบ, 3=อนุมัติ, 0=ยกเลิกการยื่น, -1=ไม่อนุมัติ, -2=ยกเลิกโดยผู้มีสิทธิ์สูง]');
            $table->integer('ot_review_by')->unsigned()->nullable()->comment('ตรวจสอบโดย');
            $table->integer('ot_approve_by')->unsigned()->nullable()->comment('อนุมัติโดย');
            $table->integer('ot_reject_by')->unsigned()->nullable()->comment('ปฏิเสธโดย');
            $table->string('ot_reject_comment', 255)->nullable()->comment('หมายเหตุการปฏิเสธ');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');

            // set foreign key
            $table->foreign('off_id')->references('off_id')->on('tb_officer');
            $table->foreign('ot_review_by')->references('off_id')->on('tb_officer');
            $table->foreign('ot_approve_by')->references('off_id')->on('tb_officer');
            $table->foreign('ot_reject_by')->references('off_id')->on('tb_officer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_over_time');
    }
}
