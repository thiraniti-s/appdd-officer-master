<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteLeaveTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_note_leave_type', function (Blueprint $table) {
            $table->increments('nl_type_id')->comment('ไอดี');
            $table->string('nl_type_name')->comment('ชื่อประเภทการลา');
            $table->tinyInteger('nl_type_status')->default(1)->comment('สถานะ [1=ใช้งาน, 0=ไม่ใช้งาน]');
            $table->timestamp('created_at')->useCurrent()->comment('เวลาสร้าง');
            $table->timestamp('updated_at')->useCurrent()->comment('เวลาอัพเดท');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_note_leave_type');
    }
}
