<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/login', function () {
    if (session()->has('login')) {
        $permission_level = session()->get('login')->off_permission_level;
        if ($permission_level == 0) return redirect('/dashboard');
        else if ($permission_level == 1) return redirect('/admin');
    }
    else {
        $username = session()->get('username');
        return view('/login', compact('username'));
    }
});
Route::get('/logout', function () {
    session()->forget('login');
    return redirect('/login');
});

// --- Admin ---

Route::middleware(['check_login_admin'])->group(function () {
    Route::get('/admin', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/admin/dashboard', function () {
        $profile = session()->get('login');
        return view('admin.dashboard', compact('profile'));
    });
    Route::get('/admin/officer', function () {
        $profile = session()->get('login');
        return view('admin.officer', compact('profile'));
    });
    Route::get('/admin/officer/{formType}/{id}', function ($formType, $id) {
        $profile = session()->get('login');
        return view('admin.officerForm', compact('profile', 'formType', 'id'));
    });
    Route::get('/admin/nl_report', function () {
        $profile = session()->get('login');
        return view('admin.report.noteleave', compact('profile'));
    });
    Route::get('/admin/noteleave', function () {
        $profile = session()->get('login');
        return view('admin.noteleave', compact('profile'));
    });
    Route::get('/admin/overtime', function () {
        $profile = session()->get('login');
        return view('admin.overtime', compact('profile'));
    });
    Route::get('/admin/project', function () {
        $profile = session()->get('login');
        return view('admin.project', compact('profile'));
    });
    Route::get('/admin/announce', function () {
        $profile = session()->get('login');
        return view('admin.announce', compact('profile'));
    });
    Route::get('/admin/master/officertype', function () {
        $profile = session()->get('login');
        return view('admin.master.officertype', compact('profile'));
    });
    Route::get('/admin/master/noteleavetype', function () {
        $profile = session()->get('login');
        return view('admin.master.noteleavetype', compact('profile'));
    });
});

// --- Officer ---

Route::middleware(['check_login_user'])->group(function () {
    Route::get('/dashboard', function () {
        $profile = session()->get('login');
        return view('dashboard', compact('profile'));
    });
    Route::get('/profile', function () {
        $profile = session()->get('login');
        return view('blank', compact('profile'));
    });
    Route::get('/noteleave', function () {
        $profile = session()->get('login');
        return view('noteleave', compact('profile'));
    });
    Route::get('/overtime', function () {
        $profile = session()->get('login');
        return view('overtime', compact('profile'));
    });
    Route::get('/changepassword', function () {
        $profile = session()->get('login');
        return view('change_password', compact('profile'));
    });
});
