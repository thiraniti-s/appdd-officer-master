<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// --- Login ---
Route::post('login',                        'User\LoginController@login');


// --- Admin ---
Route::middleware(['check_login_admin'])->prefix('admin')->namespace('Admin')->group(function () {
    Route::post('noteleave',                'NoteLeaveController@getNoteLeave');
    Route::post('noteleavemonth',           'NoteLeaveController@getNoteLeaveMonth');
    Route::post('noteleave/insert',         'NoteLeaveController@insertNoteLeave');
    Route::post('noteleave/update',         'NoteLeaveController@updateNoteLeave');
    Route::post('noteleave/status/update',  'NoteLeaveController@updateNoteLeaveStatus');
    Route::post('noteleave/request','NoteLeaveController@getNoteLeaveRequest');
    Route::post('noteleave/officer_sel','NoteLeaveController@getActiveOfficer');
    /** NOTELEAVE REPORT ZONE */
    Route::post('noteleave/report','NoteLeaveController@noteLeaveReport');
    Route::get('noteleave/type_dimension','NoteLeaveController@getNoteLeaveDimension');
    Route::post('overtime',                 'OverTimeController@getOverTime');
    Route::post('overtimemonth',            'OverTimeController@getOverTimeMonth');
    Route::post('overtime/insert',          'OverTimeController@insertOverTime');
    Route::post('overtime/status/update',   'OverTimeController@updateOverTimeStatus');
    Route::post('officer',                  'OfficerController@getOfficer');
    Route::post('officer/insert',           'OfficerController@addOfficer');
    Route::post('officer/detail',           'OfficerController@getOfficerDetail');
    Route::post('officer/update',           'OfficerController@updateOfficer');
    Route::post('officer/genaccount',       'OfficerController@generateAccount');
    Route::post('noteleavetype',            'NoteLeaveTypeController@getNoteLeaveType');
    Route::post('noteleavetype/insert',     'NoteLeaveTypeController@insertNoteLeaveType');
    Route::post('noteleavetype/update',     'NoteLeaveTypeController@updateNoteLeaveType');
    Route::post('officertype',              'OfficerTypeController@getOfficerType');
    Route::post('officertype/insert',       'OfficerTypeController@insertOfficerType');
    Route::post('officertype/update',       'OfficerTypeController@updateOfficerType');
    Route::post('dashboard/summary',        'DashboardController@getSummary');
    Route::post('project',                  'ProjectController@getProject');
    Route::post('project/insert',           'ProjectController@insertProject');
    Route::post('project/update',           'ProjectController@updateProject');
    Route::post('announce',                 'AnnounceController@getAnnounce');
    Route::post('announce/insert',          'AnnounceController@insertAnnounce');
    Route::post('announce/update',          'AnnounceController@updateAnnounce');
});

// --- Officer ---
Route::middleware(['check_login_user'])->namespace('User')->group(function () {
    Route::post('dashboard',                'DashboardController@getDashboard');
    Route::post('noteleavetype',            'NoteLeaveTypeController@getNoteLeaveType');
    Route::post('noteleave',                'NoteLeaveController@getNoteLeave');
    Route::post('noteleave/insert',         'NoteLeaveController@insertNoteLeave');
    Route::post('noteleave/status/cancel',  'NoteLeaveController@updateNoteLeaveCancel');
    Route::post('overtime',                 'OverTimeController@getOverTime');
    Route::post('overtime/insert',          'OverTimeController@insertOverTime');
    Route::post('overtime/status/cancel',   'OverTimeController@updateOverTimeCancel');
    Route::post('officer/generate',         'OfficerController@generateAccount');
    Route::post('officer/changepassword',   'OfficerController@changePassword');
    Route::post('officer',                  'OfficerController@getOfficer');
});

// --- Debug ---

Route::get('test',                          'TempController@test');
