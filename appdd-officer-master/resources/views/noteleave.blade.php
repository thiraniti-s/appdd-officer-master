@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        ใบลางาน
        <small>ใบลางานของคุณ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/profile')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('noteleave')}}">ใบลางาน</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <!-- Box Request -->
        <div class="col-xs-12">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">รายการยื่นใบลา</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="150">ประเภทใบลา</th>
                  <th class="text-td-center" width="150">ประเภทเวลา</th>
                  <th class="text-td-center" width="180">วันที่</th>
                  <th class="text-td-center" width="150">เวลา</th>
                  <th class="text-td-center">บันทึกการลา</th>
                  <th class="text-td-center" width="80">สถานะ</th>
                  <th class="text-td-center" width="60">ดูเพิ่ม</th>
                  <th class="text-td-center" width="60">ยกเลิก</th>
                </tr>
                <tr v-for="(data, index) in lists_request">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.note_leave_type_data.nl_type_name }}
                  </td>
                  <td class="text-td-center">
                    <span v-if="data.nl_time_type==1">ลาเต็มวัน</span>
                    <span v-else>ลาบางชั่วโมง</span>
                  </td>
                  <td class="text-td-left" v-if="data.nl_leave_start != data.nl_leave_end">
                    @{{ _momentDate(data.nl_leave_start) }} - @{{ _momentDate(data.nl_leave_end) }}
                  </td>
                  <td class="text-td-left" v-else>
                    @{{ _momentDate(data.nl_leave_start) }}
                  </td>
                  <td class="text-td-center" v-if="data.nl_time_type==1">
                    -
                  </td>
                  <td class="text-td-center" v-else>
                    @{{ _momentTime(data.nl_time_start) }} - @{{ _momentTime(data.nl_time_end) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.nl_note }}
                  </td>
                  <td class="text-td-center" v-if="data.nl_status==1">
                    <span class="badge bg-gray">Waiting</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==2">
                    <span class="badge bg-yellow">InReview</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==3">
                    <span class="badge bg-green">Approve</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==-1">
                    <span class="badge bg-red">Reject</span>
                  </td>
                  <td class="text-td-center" v-else>
                    <span class="badge bg-gray">Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_cancel(data)" v-if="data.nl_status==1">
                      <i class="fa fa-trash"></i>
                    </button>
                    <span v-else>-</span>
                  </td>
                </tr>
                <tr v-if="lists_request.length == 0">
                  <td colspan="9" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box Request -->

        <!-- Box Request -->
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">รายการยื่นใบลาล่วงหน้า</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="150">ประเภทใบลา</th>
                  <th class="text-td-center" width="150">ประเภทเวลา</th>
                  <th class="text-td-center" width="180">วันที่</th>
                  <th class="text-td-center" width="150">เวลา</th>
                  <th class="text-td-center">บันทึกการลา</th>
                  <th class="text-td-center" width="80">สถานะ</th>
                  <th class="text-td-center" width="60">ดูเพิ่ม</th>
                  <th class="text-td-center" width="60">ยกเลิก</th>
                </tr>
                <tr v-for="(data, index) in lists_request">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.note_leave_type_data.nl_type_name }}
                  </td>
                  <td class="text-td-center">
                    <span v-if="data.nl_time_type==1">ลาเต็มวัน</span>
                    <span v-else>ลาบางชั่วโมง</span>
                  </td>
                  <td class="text-td-left" v-if="data.nl_leave_start != data.nl_leave_end">
                    @{{ _momentDate(data.nl_leave_start) }} - @{{ _momentDate(data.nl_leave_end) }}
                  </td>
                  <td class="text-td-left" v-else>
                    @{{ _momentDate(data.nl_leave_start) }}
                  </td>
                  <td class="text-td-center" v-if="data.nl_time_type==1">
                    -
                  </td>
                  <td class="text-td-center" v-else>
                    @{{ _momentTime(data.nl_time_start) }} - @{{ _momentTime(data.nl_time_end) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.nl_note }}
                  </td>
                  <td class="text-td-center" v-if="data.nl_status==1">
                    <span class="badge bg-gray">Waiting</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==2">
                    <span class="badge bg-yellow">InReview</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==3">
                    <span class="badge bg-green">Approve</span>
                  </td>
                  <td class="text-td-center" v-else-if="data.nl_status==-1">
                    <span class="badge bg-red">Reject</span>
                  </td>
                  <td class="text-td-center" v-else>
                    <span class="badge bg-gray">Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_cancel(data)" v-if="data.nl_status==1">
                      <i class="fa fa-trash"></i>
                    </button>
                    <span v-else>-</span>
                  </td>
                </tr>
                <tr v-if="lists_request.length == 0">
                  <td colspan="9" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box Request -->
        
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">รายการใบลา - <?php echo (new DateTime())->format('M Y'); ?></h3>
            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                  <tr>
                    <th class="text-td-center" width="40">#</th>
                    <th class="text-td-center" width="150">ประเภทใบลา</th>
                    <th class="text-td-center" width="150">ประเภทเวลา</th>
                    <th class="text-td-center" width="180">วันที่</th>
                    <th class="text-td-center" width="150">เวลา</th>
                    <th class="text-td-center" width="100">คิดเป็น (ชม.)</th>
                    <th class="text-td-center">บันทึกการลา</th>
                    <th class="text-td-center" width="80">สถานะ</th>
                    <th class="text-td-center" width="60">ดูเพิ่ม</th>
                  </tr>
                  <tr v-for="(data, index) in lists">
                    <td class="text-td-center">
                      @{{ index+1 }}
                    </td>
                    <td class="text-td-center">
                      @{{ data.note_leave_type_data.nl_type_name }}
                    </td>
                    <td class="text-td-center">
                      <span v-if="data.nl_time_type==1">ลาเต็มวัน</span>
                      <span v-else>ลาบางชั่วโมง</span>
                    </td>
                    <td class="text-td-left" v-if="data.nl_leave_start != data.nl_leave_end">
                      @{{ _momentDate(data.nl_leave_start) }} - @{{ _momentDate(data.nl_leave_end) }}
                    </td>
                    <td class="text-td-left" v-else>
                      @{{ _momentDate(data.nl_leave_start) }}
                    </td>
                    <td class="text-td-center" v-if="data.nl_time_type==1">
                      -
                    </td>
                    <td class="text-td-center" v-else>
                      @{{ _momentTime(data.nl_time_start) }} - @{{ _momentTime(data.nl_time_end) }}
                    </td>
                    <td class="text-td-center" v-if="data.note_leave_type_data.nl_type_name=='ON-SITE'">
                      ---
                    </td>
                    <td class="text-td-center" v-else-if="data.note_leave_type_data.nl_type_name=='ลากิจกรรม'">
                      ---
                    </td>
                    <td class="text-td-center" v-else-if="data.nl_time_type==1">
                      <span v-if="_momentDiffDateWork(data.nl_leave_end, data.nl_leave_start)==0">8</span>
                      <span v-else>8 x @{{ _momentDiffDateWork(data.nl_leave_end, data.nl_leave_start) + 1 }} = @{{ (_momentDiffDateWork(data.nl_leave_end, data.nl_leave_start) + 1) * 8 }}</span>
                    </td>
                    <td class="text-td-center" v-else>
                      @{{ _momentDiffTimeWork(data.nl_time_start, data.nl_time_end) }}
                    </td>
                    <td class="text-td-left" v-if="data.nl_status==-1">
                      @{{ data.nl_note }}@{{ !! data.nl_reject_comment ? ' ==> '+data.nl_reject_comment : '' }}
                    </td>
                    <td class="text-td-left" v-else>
                      @{{ data.nl_note }}
                    </td>
                    <td class="text-td-center" v-if="data.nl_status==3">
                      <span class="badge bg-green">Approve</span>
                    </td>
                    <td class="text-td-center" v-else-if="data.nl_status==-1">
                      <span class="badge bg-red">Reject</span>
                    </td>
                    <td class="text-td-center" v-else>
                      <span class="badge bg-gray">Unknown</span>
                    </td>
                    <td class="text-td-center">
                      <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                        <i class="fa fa-search"></i>
                      </button>
                    </td>
                  </tr>
                  <tr v-if="lists.length == 0">
                    <td colspan="9" class="empty-data">
                      -- Data Not Found --
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ฟอร์มยื่นใบลา</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="nl_type">ประเภทใบลา</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="nl_type_id">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option v-for="nl_type in noteleave_types" :value="nl_type.nl_type_id">
                      @{{ nl_type.nl_type_name }}
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="nl_time_type">ประเภทเวลา</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="nl_time_type">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option value="1">ลาเต็มวัน</option>
                    <option value="2">ลาบางชั่วโมง</option>
                  </select>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="">บันทึกการลา</label>
                  <input type="text" class="form-control" placeholder="" v-model="data.nl_note">
                </div>
                <div class="form-group" id="div_date1">
                  <label for="exampleInputEmail1">วันที่ (ลาป่วยสามารถเลือกวันย้อนหลังได้ + ส่งใบรับรองแพทย์)</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker datepicker1" id="nl_leave_start1" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker datepicker1" id="nl_leave_end1" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group" id="div_date2">
                  <label for="exampleInputEmail1">วันที่ (ไม่สามารถเลือกย้อนหลังได้)</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker datepicker2" id="nl_leave_start2" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker datepicker2" id="nl_leave_end2" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="div_time">
                  <div class="form-group">
                    <label>เวลา</label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" id="nl_time_start" readonly>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" id="nl_time_end" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" @click="_submit()">ยื่นใบลา</button>
              <button type="submit" class="btn btn-default" @click="_clear()">เคลียร์</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker1').datepicker({
        autoclose: true,
        todayHighlight: true,
        daysOfWeekDisabled: [0,6],
        startDate: moment().date(1).format('YYYY-MM-DD'),
        endDate: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd'
      });
      $('.datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true,
        daysOfWeekDisabled: [0,6],
        startDate: moment().date(1).format('YYYY-MM-DD'),
        endDate: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd'
      });
      $('#nl_time_start').timepicker({
        defaultTime: '09:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false
      });
      $('#nl_time_end').timepicker({
        defaultTime: '12:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false
      });

      $('#div_time').hide();
      $('#nl_time_type').change(function () {
        if (parseInt($('#nl_time_type').val()) === 2) {
          $('#div_time').show();
        }
        else {
          $('#div_time').hide();
        }
      });

      $('#div_date1').hide();
      $('#nl_type_id').change(function () {
        if (parseInt($('#nl_type_id').val()) === 1) {
          $('#div_date1').show();
          $('#div_date2').hide();
        }
        else {
          $('#div_date1').hide();
          $('#div_date2').show();
        }
      });
    });
    
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        lists_request: [],
        noteleave_types: [],
        off_id: '{{ $profile->off_id }}',
        off_firstname: '{{ $profile->off_firstname }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 100
        },
        data: {
          nl_type_id: '',
          nl_time_type: '',
          nl_leave_start: moment().format('YYYY-MM-DD'),
          nl_leave_end: moment().format('YYYY-MM-DD'),
          nl_time_start: '09:00',
          nl_time_end: '12:00',
          nl_note: ''
        }
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_noteleave_type();
          this._get(true);
        })
      },
      methods: {
        _get_noteleave_type: function () {
          let _data = {
            off_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/noteleavetype')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              this.noteleave_types = body.data.result;
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            off_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          // _log(_data);
          axios.post('{{url('/api/noteleave')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;

              this.lists = result.filter((data) => {
                if (data.nl_status === 3 || data.nl_status === -1) {
                  return data
                }
              });
              this.lists_request = result.filter((data) => {
                if (data.nl_status === 1 || data.nl_status === 2) {
                  return data
                }
              });
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _clear: function () {
          $('.select2').val('').trigger('change');
          this.data.nl_leave_start = moment().format('YYYY-MM-DD');
          this.data.nl_leave_end = moment().format('YYYY-MM-DD');
          this.data.nl_time_start = '09:00';
          this.data.nl_time_end = '12:00';
          this.data.nl_note = '';
          $('#nl_leave_start1').val(moment().format('YYYY-MM-DD'));
          $('#nl_leave_end1').val(moment().format('YYYY-MM-DD'));
          $('#nl_leave_start2').val(moment().format('YYYY-MM-DD'));
          $('#nl_leave_end2').val(moment().format('YYYY-MM-DD'));
          $('#nl_time_start').val('09:00');
          $('#nl_time_end').val('12:00');
        },
        _validate: function () {
          this.data.nl_type_id = $('#nl_type_id').val();
          this.data.nl_time_type = $('#nl_time_type').val();
          if (parseInt(this.data.nl_type_id) === 1) {
            this.data.nl_leave_start = $('#nl_leave_start1').val();
            this.data.nl_leave_end = $('#nl_leave_end1').val();
          }
          else {
            this.data.nl_leave_start = $('#nl_leave_start2').val();
            this.data.nl_leave_end = $('#nl_leave_end2').val();
          }
          this.data.nl_time_start = $('#nl_time_start').val();
          this.data.nl_time_end = $('#nl_time_end').val();

          if (parseInt(this.data.nl_time_type) === 2) {
            this.data.nl_leave_end = this.data.nl_leave_start;
            $('#nl_leave_end').val(this.data.nl_leave_end);
          }

          if (!_nextIsNotValid(this.data.nl_type_id, '', 'กรุณาเลือกประเภทใบลา')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_time_type, '', 'กรุณาเลือกประเภทเวลา')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_note, '', 'กรุณากรอกบันทึกการลา')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_leave_start, '', 'กรุณาเลือกวันที่เริ่มลา')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_leave_end, '', 'กรุณาเลือกวันสิ้นสุดการลา')) {
            return false;
          }
          if (!moment(this.data.nl_leave_start).isSameOrBefore(this.data.nl_leave_end)) {
            swal({
              text: 'เลือกวันที่ไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
              icon: 'warning'
            });
            return false;
          }
          
          if (parseInt(this.data.nl_time_type) === 2) {
            if (!_nextIsNotValid(this.data.nl_time_start, '', 'กรุณาเลือกเวลาเริ่มลา')) {
              return false;
            }
            if (!_nextIsNotValid(this.data.nl_time_end, '', 'กรุณาเลือกเวลาสิ้นสุดการลา')) {
              return false;
            }
            if (!moment(this.data.nl_time_start, 'HH:ss').isBefore(moment(this.data.nl_time_end, 'HH:ss'))) {
              swal({
                text: 'เลือกเวลาไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
                icon: 'warning'
              });
              return false;
            }
          }
          
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการส่งใบลา',
              text: 'ต้องการส่งใบลาพร้อมกับข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                let data;
                if (parseInt(this.data.nl_time_type) === 1) {
                  data = {
                    off_id: this.off_id,
                    nl_type_id: this.data.nl_type_id,
                    nl_time_type: this.data.nl_time_type,
                    nl_note: this.data.nl_note,
                    nl_leave_start: this.data.nl_leave_start,
                    nl_leave_end: this.data.nl_leave_end
                  };
                }
                else {
                  data = {
                    off_id: this.off_id,
                    nl_type_id: this.data.nl_type_id,
                    nl_time_type: this.data.nl_time_type,
                    nl_note: this.data.nl_note,
                    nl_leave_start: this.data.nl_leave_start,
                    nl_leave_end: this.data.nl_leave_end,
                    nl_time_start: this.data.nl_time_start,
                    nl_time_end: this.data.nl_time_end
                  };
                }
                // _log(data);
                axios.post('{{url('/api/noteleave/insert')}}', data).then(response => {
                  let body = response.data;
                  // _log(body);
                  _nextCloseLoading();
                  if (body.status === 'ok') {
                    swal({
                      title: 'ทำรายการสำเร็จ',
                      text: 'ส่งข้อมูลถึงผู้ดูแลระบบแล้ว โปรดรอการอนุมัติ',
                      icon: 'success',
                      closeOnClickOutside: false
                    }).then(ok => {
                      this._clear();
                      this._get(true);
                    });
                  }
                  else {
                    _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                  }
                }).catch(error => {
                  _nextCloseLoading();
                  _nextAlert(error.message, 'err');
                });
              }
            });
          }
        },
        _cancel: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบลาเป็น "ยกเลิก" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              let _data = {
                off_id: this.off_id,
                nl_id: data.nl_id
              };
              axios.post('{{url('/api/noteleave/status/cancel')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        }
      }
    });
  </script>
@endsection