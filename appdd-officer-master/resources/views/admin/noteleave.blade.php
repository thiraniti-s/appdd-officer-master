@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Note Leave
        <small>note leave of application DD officer</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('/admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('/admin/noteleave')}}">Note Leave</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <!-- Box Request -->
        <div class="col-xs-12" v-if="lists_request.length > 0">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Request :: Note Leave</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="170">Officer Name</th>
                  <th class="text-td-center" width="150">Type</th>
                  <th class="text-td-center" width="180">Date</th>
                  <th class="text-td-center" width="150">Time</th>
                  <th class="text-td-center">Note / Comment</th>
                  <th class="text-td-center" width="80">Status</th>
                  <th class="text-td-center" width="60">View</th>
                  <th class="text-td-center" width="60">Edit</th>
                  <th class="text-td-center" width="180">Action</th>
                </tr>
                <tr v-for="(data, index) in lists_request">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.officer_data.off_prefixname }}@{{ data.officer_data.off_firstname }} (@{{ data.officer_data.off_nickname }})
                  </td>
                  <td class="text-td-center" v-if="data.nl_time_type==1">
                    @{{ data.note_leave_type_data.nl_type_name }} | เต็มวัน
                  </td>
                  <td class="text-td-center" v-else>
                    @{{ data.note_leave_type_data.nl_type_name }} | บางชั่วโมง
                  </td>
                  <td class="text-td-left" v-if="data.nl_leave_start != data.nl_leave_end">
                    @{{ _momentDate(data.nl_leave_start) }} - @{{ _momentDate(data.nl_leave_end) }}
                  </td>
                  <td class="text-td-left" v-else>
                    @{{ _momentDate(data.nl_leave_start) }}
                  </td>
                  <td class="text-td-center" v-if="data.nl_time_type==1">
                    -
                  </td>
                  <td class="text-td-center" v-else>
                    @{{ _momentTime(data.nl_time_start) }} - @{{ _momentTime(data.nl_time_end) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.nl_note }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-gray" v-if="data.nl_status==1">Waiting</span>
                    <span class="badge bg-yellow" v-else-if="data.nl_status==2">InReview</span>
                    <span class="badge bg-gray" v-else>Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                    <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-gray" v-if="data.nl_status==1">-</span>
                    <button type="button" class="btn btn-block btn-xs btn-default" v-else-if="data.nl_status==2" @click="_edit(data)">
                    <i class="fa fa-pencil"></i>
                    </button>
                  </td>
                  <td class="text-td-center" v-if="data.nl_status!==3">
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-warning" @click="_review(data)" v-if="data.nl_status==1">
                        <i class="fa fa-circle-o"></i> RW
                      </button>
                    </div>
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-success" @click="_approve(data)" v-if="data.nl_status==2 || data.nl_status==1">
                        <i class="fa fa-check"></i> AP
                      </button>
                    </div>
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-danger" @click="_reject(data)" v-if="data.nl_status==2 || data.nl_status==1">
                        <i class="fa fa-times"></i> RJ
                      </button>
                    </div>
                  </td>
                  <td class="text-td-center" v-else>
                    -
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box Request -->
        
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title" style="line-height: 32px;">List :: Note Leave</h3>
                <div class="form-group pull-right col-md-5 margin-bottom-none" style="padding-right: 0;">
                  <select class="form-control select2" style="width: 100%" v-model="monthsSelected" tabindex="-1" aria-hidden="true" id="nl_month">
                      <option v-for="month in noteleave_months" :value="month.idkey">
                        @{{ month.value }}
                      </option>
                  </select>
                </div>
                <div class="form-group pull-right col-md-3 margin-bottom-none" style="padding-right: 0;">
                  <select class="form-control select2" style="width: 100%" v-model="officerSelected" tabindex="-1" aria-hidden="true" id="nl_officer">
                    <option value="all">ทั้งหมด</option>
                    <option v-for="off in officer_list" :value="off.idkey">
                      @{{ off.full_name }}
                    </option>
                  </select>
                </div>

            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                  <tr>
                    <th class="text-td-center" width="40">#</th>
                    <th class="text-td-center" width="170">Officer Name</th>
                    <th class="text-td-center" width="100">Type</th>
                    <th class="text-td-center" width="130">Time Type</th>
                    <th class="text-td-center" width="180">Date</th>
                    <th class="text-td-center" width="150">Time</th>
                    <th class="text-td-center" width="100">CalHour</th>
                    <th class="text-td-center">Note / Comment</th>
                    <th class="text-td-center" width="80">Status</th>
                    <th class="text-td-center" width="60">View</th>
                    <th class="text-td-center" width="60">Delete</th>
                  </tr>
                  <tr v-for="(data, index) in lists">
                    <td class="text-td-center">
                      @{{ index+1 }}
                    </td>
                    <td class="text-td-left">
                      @{{ data.officer_data.off_prefixname }}@{{ data.officer_data.off_firstname }} (@{{ data.officer_data.off_nickname }})
                    </td>
                    <td class="text-td-center text-danger" v-if="data.note_leave_type_data.nl_type_level > 1">
                      <strong>@{{ data.note_leave_type_data.nl_type_name }}</strong>
                    </td>
                    <td class="text-td-center" v-else>
                      @{{ data.note_leave_type_data.nl_type_name }}
                    </td>
                    <td class="text-td-center">
                      <span v-if="data.nl_time_type==1">ลาเต็มวัน</span>
                      <span v-else>ลาบางชั่วโมง</span>
                    </td>
                    <td class="text-td-left" v-if="data.nl_leave_start != data.nl_leave_end">
                      @{{ _momentDate(data.nl_leave_start) }} - @{{ _momentDate(data.nl_leave_end) }}
                    </td>
                    <td class="text-td-left" v-else>
                      @{{ _momentDate(data.nl_leave_start) }}
                    </td>
                    <td class="text-td-center" v-if="data.nl_time_type==1">
                      -
                    </td>
                    <td class="text-td-center" v-else>
                      @{{ _momentTime(data.nl_time_start) }} - @{{ _momentTime(data.nl_time_end) }}
                    </td>
                    <td class="text-td-center" v-if="data.note_leave_type_data.nl_type_name=='ON-SITE'">
                      ---
                    </td>
                    <td class="text-td-center" v-else-if="data.note_leave_type_data.nl_type_name=='ลากิจกรรม'">
                      ---
                    </td>
                    <td class="text-td-center" v-else-if="data.nl_time_type==1">
                      <span v-if="_momentDiffDateWork(data.nl_leave_end, data.nl_leave_start)==0">8</span>
                      <span v-else>8 x @{{ _momentDiffDateWork(data.nl_leave_end, data.nl_leave_start) + 1 }} = @{{ (_momentDiffDateWork(data.nl_leave_end, data.nl_leave_start) + 1) * 8 }}</span>
                    </td>
                    <td class="text-td-center" v-else>
                      @{{ _momentDiffTimeWork(data.nl_time_start, data.nl_time_end) }}
                    </td>
                    <td class="text-td-left" v-if="data.nl_status==-1">
                      @{{ data.nl_note }}@{{ !! data.nl_reject_comment ? ' ==> '+data.nl_reject_comment : '' }}
                    </td>
                    <td class="text-td-left" v-else>
                      @{{ data.nl_note }}
                    </td>
                    <td class="text-td-center">
                      <span class="badge bg-green" v-if="data.nl_status==3">Approve</span>
                      <span class="badge bg-red" v-else-if="data.nl_status==-1">Reject</span>
                      <span class="badge bg-gray" v-else>Unknown</span>
                    </td>
                    <td class="text-td-center">
                      <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                        <i class="fa fa-search"></i>
                      </button>
                    </td>
                   </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_delete(data)">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                  </tr>
                  <tr v-if="lists.length == 0">
                    <td colspan="10" class="empty-data">
                      -- Data Not Found --
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Note Leave</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="officer">Officer</label>
                  <select v-if="data.nl_status==2" class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_id" disabled>
                    <option selected="selected" value="">-- เลือก --</option>
                    <option v-for="off in officers" :value="off.off_id">
                      @{{ off.off_prefixname }}@{{ off.off_firstname }} @{{ off.off_lastname }} (@{{ off.off_nickname }})
                    </option>
                  </select>
                  <select v-else class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_id">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option v-for="off in officers" :value="off.off_id">
                      @{{ off.off_prefixname }}@{{ off.off_firstname }} @{{ off.off_lastname }} (@{{ off.off_nickname }})
                    </option>
                  </select>
                </div>
          
                <div class="form-group">
                  <label for="nl_time_type">Leave Time Type</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="nl_time_type">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option value="1">ลาเต็มวัน</option>
                    <option value="2">ลาบางชั่วโมง</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker1" id="nl_leave_start" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker2" id="nl_leave_end" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nl_type">Leave Type</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="nl_type_id">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option v-for="nl_type in noteleave_types" :value="nl_type.nl_type_id">
                      @{{ nl_type.nl_type_name }}
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Note</label>
                  <input type="text" class="form-control" placeholder="" v-model="data.nl_note">
                </div>
                <div id="div_time">
                  <div class="form-group">
                    <label>Time</label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="nl_time_start" value="09:00" readonly>
                        </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="nl_time_end" value="12:00" readonly>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer" id="demo2">
              <button type="button" class="btn btn-primary" @click="_submit()">Submit</button>
              <button type="button" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker1').datepicker({
        autoclose: true,
        todayHighlight: true,
        daysOfWeekDisabled: [0,6],
        startDate: moment().date(1).format('YYYY-MM-DD'),
        endDate: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd'
      });
      $('.datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true,
        daysOfWeekDisabled: [0,6],
        startDate: moment().date(1).format('YYYY-MM-DD'),
        endDate: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd'
      });
      $('#nl_time_start').timepicker({
        defaultTime: '09:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false,
      });
      $('#nl_time_end').timepicker({
        defaultTime: '12:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false
      });

      $('#div_time').hide();
      $('#nl_time_type').change(function() {
        if (parseInt($('#nl_time_type').val()) === 2) {
          $('#div_time').show();
        }
        else {
          $('#div_time').hide();
        }
      });
      
      $('#nl_month').change(function () {
        let val = $('#nl_month').val();
        if (val) {
          let ex = val.split("-");
          main._set_month_year(ex[1], ex[0]);
          main.monthsSelected = val;
        }
        main.officerSelected = "all";
        main.officer_list = null;
        main._set_officer("all");
        main._get(true);
        main._get_officer_select(true);
      })

      $('#nl_officer').change(function () {
        let val = $('#nl_officer').val();
        if (val) {
          main.officerSelected = val;
          main._set_officer(val);
        }
        main._get(true);
      })
    });
    
    let main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        lists_request: [],
        officers: [],
        noteleave_officers:[],
        noteleave_types: [],
        noteleave_months: [],
        officer_list:[],
        off_id: '{{ $profile->off_id }}',
        nl_month: moment().format('M'),
        nl_year: moment().format('YYYY'),
        is_clear: false,
        monthsSelected: '',
        officerSelected: 'all',
        officer_id_sel:'',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 100
        },
        data: {
          nl_id:'',
          off_id: '',
          nl_type_id: '',
          nl_time_type: '',
          nl_leave_start: moment().date(1).format('YYYY-MM-DD'),
          nl_leave_end: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
          nl_time_start: '09:00',
          nl_time_end: '12:00',
          nl_note: '',
          nl_status: ''
        },
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_officer();
          this._get_noteleave_type();
          this._get_noteleave_months();
          this._get(true);
          this._get_officer_select(true);
        })
      },
      methods: {
        _set_month_year: function ($month, $year) {
          this.nl_month = $month;
          this.nl_year = $year;
        },
        _set_officer: function ($officer) {
          this.officer_id_sel = $officer;
        },
        _get_officer: function () {
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/admin/officer')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.officers = result.filter((data) => {
                if (data.off_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },

        _get_noteleave_type: function () {
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/admin/noteleavetype')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.noteleave_types = result.filter((data) => {
                if (data.nl_type_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get_noteleave_months: function () {
          axios.post('{{url('/api/admin/noteleavemonth')}}').then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              this.noteleave_months = body.data;
              this.monthsSelected = this.noteleave_months.filter((data) => {
                if (data.selected === 'y') {
                  return data.idkey;
                }
              })[0].idkey;
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
            nl_month: this.nl_month,
            nl_year: this.nl_year,
            officer_id: this.officer_id_sel,
          };

          // _log(_data);
          axios.post('{{url('/api/admin/noteleave')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;
              this.lists = result.filter((data) => {
                if (data.nl_status === 3 || data.nl_status === -1) {
                  return data
                }
              });
            } else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });

          axios.post('{{url('/api/admin/noteleave/request')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;
              this.lists_request = result.filter((data) => {
                if (data.nl_status === 1 || data.nl_status === 2) {
                  return data
                }
              });
            } else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _get_officer_select: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
            nl_month: this.nl_month,
            nl_year: this.nl_year,
            officer_id: this.officer_id_sel,
          };
          axios.post('{{url('/api/admin/noteleave/officer_sel')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              let result = body.data;
              this.officer_list = result;
              console.log(this.officer_list);
            } else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _clear: function () {
          // $('.select2').val('').trigger('change');
          $('#off_id').val('').trigger('change');
          $('#nl_time_type').val('').trigger('change');
          $('#nl_type_id').val('').trigger('change');
          this.data.nl_leave_start = moment().format('YYYY-MM-DD');
          this.data.nl_leave_end = moment().format('YYYY-MM-DD');
          this.data.nl_time_start = '09:00';
          this.data.nl_time_end = '18:00';
          this.data.nl_note = '';
          $('#nl_leave_start').val(moment().format('YYYY-MM-DD'));
          $('#nl_leave_end').val(moment().format('YYYY-MM-DD'));
          $('#nl_time_start').val('09:00');
          $('#nl_time_end').val('12:00');
        },
        _validate: function () {
          this.data.off_id = $('#off_id').val();
          this.data.nl_type_id = $('#nl_type_id').val();
          this.data.nl_time_type = $('#nl_time_type').val();
          this.data.nl_leave_start = $('#nl_leave_start').val();
          this.data.nl_leave_end = $('#nl_leave_end').val();
          this.data.nl_time_start = $('#nl_time_start').val();
          this.data.nl_time_end = $('#nl_time_end').val();

          if (parseInt(this.data.nl_time_type) === 2) {
            this.data.nl_leave_end = this.data.nl_leave_start;
            $('#nl_leave_end').val(this.data.nl_leave_end);
          }

          if (!_nextIsNotValid(this.data.off_id, '', 'Please select officer')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_type_id, '', 'Please select leave type')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_time_type, '', 'Please select leave time type')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_note, '', 'Please input note')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_leave_start, '', 'Please choose date start')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.nl_leave_end, '', 'Please choose date end')) {
            return false;
          }
          if (!moment(this.data.nl_leave_start).isSameOrBefore(this.data.nl_leave_end)) {
            swal({
              text: 'Date is wrong, pls check again',
              icon: 'warning'
            });
            return false;
          }
          if (parseInt(this.data.nl_time_type) === 2) {
            if (!_nextIsNotValid(this.data.nl_time_start, '', 'Please choose time start')) {
              return false;
            }
            if (!_nextIsNotValid(this.data.nl_time_end, '', 'Please choose time end')) {
              return false;
            }
            if (!moment(this.data.nl_time_start, 'HH:ss').isBefore(moment(this.data.nl_time_end, 'HH:ss'))) {
              swal({
                text: 'Time is wrong, pls check again',
                icon: 'warning'
              });
              return false;
            }
          }
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการบันทึก',
              text: 'ต้องการบันทึกข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                if (this.data.nl_id === '') {
                  let _data = {
                    admin_id: this.off_id,
                    off_id: this.data.off_id,
                    nl_type_id: this.data.nl_type_id,
                    nl_time_type: this.data.nl_time_type,
                    nl_note: this.data.nl_note,
                    nl_leave_start: this.data.nl_leave_start,
                    nl_leave_end: this.data.nl_leave_end,
                    nl_time_start: this.data.nl_time_start,
                    nl_time_end: this.data.nl_time_end
                  };
                  // _log(_data);
                  axios.post('{{url('/api/admin/noteleave/insert')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success',
                        closeOnClickOutside: false
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    } else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
                // update data
                else {
                  let _data = {
                    admin_id: this.off_id,
                    nl_id: this.data.nl_id,
                    off_id: this.data.off_id,
                    nl_type_id: this.data.nl_type_id,
                    nl_time_type: this.data.nl_time_type,
                    nl_note: this.data.nl_note,
                    nl_leave_start: this.data.nl_leave_start,
                    nl_leave_end: this.data.nl_leave_end,
                    nl_time_start: this.data.nl_time_start,
                    nl_time_end: this.data.nl_time_end,
                    nl_status: parseInt(this.data.nl_status)
                  };
                  // _log(_data);
                  axios.post('{{url('/api/admin/noteleave/update')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success',
                        closeOnClickOutside: false
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    } else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
              }
            });
          }
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        },
        _edit: function (data) {
          this.data.nl_id = data.nl_id;
          this.data.off_id = data.off_id;
          $('#off_id').val(data.off_id).trigger('change');
          this.data.nl_type_id = data.nl_type_id;
          $('#nl_type_id').val(data.nl_type_id).trigger('change');
          this.data.nl_time_type = data.nl_time_type;
          $('#nl_time_type').val(data.nl_time_type).trigger('change');
          this.data.nl_note = data.nl_note;
          this.data.nl_leave_start = moment(data.nl_leave_start).format('YYYY-MM-DD');
          $('#nl_leave_start').val(moment(data.nl_leave_start).format('YYYY-MM-DD'));
          this.data.nl_leave_end = moment(data.nl_leave_end).format('YYYY-MM-DD');
          $('#nl_leave_end').val(moment(data.nl_leave_end).format('YYYY-MM-DD'));
          this.data.nl_time_start = data.nl_time_start;
          $('#nl_time_start').val(data.nl_time_start);
          this.data.nl_time_end = data.nl_time_end;
          $('#nl_time_end').val(data.nl_time_end);
          this.data.nl_status = data.nl_status;
        },
        _delete: function (data) {
          swal({
            title: 'ยืนยันการลบ',
            text: 'ต้องการลบข้อมูล หรือไม่ ?',
            icon: 'info',
            buttons: ['Cancel', 'Confirm']
          }).then(confirm => {
            if (confirm) {
              let _data = {
                admin_id: this.off_id,
                nl_id: data.nl_id,
                off_id: data.off_id,
                nl_type_id: data.nl_type_id,
                nl_time_type: data.nl_time_type,
                nl_note: data.nl_note,
                nl_leave_start: data.nl_leave_start,
                nl_leave_end: data.nl_leave_end,
                nl_time_start: data.nl_time_start,
                nl_time_end: data.nl_time_end,
                nl_status: 0
              };
              // _log(_data);
              axios.post('{{url('/api/admin/noteleave/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                } else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _review: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบลาเป็น "In Review" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                nl_id: data.nl_id,
                nl_status: 2
              };
              axios.post('{{url('/api/admin/noteleave/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                } else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _approve: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบลาเป็น "Approve" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                nl_id: data.nl_id,
                nl_status: 3
              };
              axios.post('{{url('/api/admin/noteleave/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                } else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _reject: function (data) {
          swal({
            content: 'input',
            title: 'Reject Comment',
            text: 'Write reject comment here:',
            icon: 'warning',
            buttons: [true, 'Reject'],
            dangerMode: true,
            closeOnClickOutside: false
          }).then(message => {
            if (message) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                nl_id: data.nl_id,
                nl_status: -1,
                nl_reject_comment: message
              };
              axios.post('{{url('/api/admin/noteleave/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                } else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            } else {
              _nextAlert('ยกเลิกการทำรายการ', 'war');
            }
          });
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        }
      }
    });
  </script>
@endsection