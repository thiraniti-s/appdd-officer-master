@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Dashboard
        <small>application DD officer dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('/admin')}}">Dashboard</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green-gradient">
            <div class="inner">
              <h3>
                <span v-if="summary.count_officers!=null">@{{ summary.count_officers }}</span>
                <span v-else>&nbsp;</span>
              </h3>
              <p>Officers</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="{{ url('admin/officer') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow-gradient">
            <div class="inner">
              <h3>
                <span v-if="summary.count_request_noteleaves!=null || summary.count_request_overtimes!=null">@{{ summary.count_request_noteleaves }} / @{{ summary.count_request_overtimes }}</span>
                <span v-else>&nbsp;</span>
              </h3>
              <p>Requested NL / OT</p>
            </div>
            <div class="icon">
              <i class="ion ion-flash"></i>
            </div>
            <a href="{{ url('admin/noteleave') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
  
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-blue-gradient">
            <div class="inner">
              <h3>
                <span v-if="summary.count_approve_noteleaves!=null">@{{ summary.count_approve_noteleaves }}</span>
                <span v-else>&nbsp;</span>
              </h3>
              <p>Approved Note Leaves</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-checkbox-outline"></i>
            </div>
            <a href="{{ url('admin/noteleave') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
  
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red-gradient">
            <div class="inner">
              <h3>
                <span v-if="summary.count_approve_overtimes!=null">@{{ summary.count_approve_overtimes }}</span>
                <span v-else>&nbsp;</span>
              </h3>
              <p>Approved Over Times</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="{{ url('admin/overtime') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        summary: {},
        off_id: '{{ $profile->off_id }}',
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get(true);
        });
      },
      methods: {
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id
          };
          // _log(_data);
          axios.post('{{url('/api/admin/dashboard/summary')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              this.summary = body.data.result;
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
      }
    });
  </script>
@endsection