@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Officer
        <small>form of officer in application DD</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> Home</li>
        <li class="active"><a href="{{url('/admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('/admin/officer')}}">Officer</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
        
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Officer</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
 
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="off_type_id">Type</label>
                      <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_type_id" v-model="form.off_type_id">
                        <option value="">--</option>
                        <option v-for="off_type in officer_types" :value="off_type.off_type_id">
                          @{{ off_type.off_type_name }}
                        </option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label for="">Officer Code</label>
                      <input type="text" class="form-control" placeholder="APPDD-0001" v-model="form.off_code">
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label for="off_prefixname">Prefix</label>
                      <select class="form-control " style="width: 100%" tabindex="-1" aria-hidden="true" id="off_prefixname" v-model="form.off_prefixname">
                        <option selected="selected" value="">--</option>
                        <option value="นาย">นาย</option>
                        <option value="นาง">นาง</option>
                        <option value="นางสาว">นางสาว</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="">Firstname</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_firstname">
                    </div>
                    <div class="col-md-4">
                      <label for="">Lastname</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_lastname">
                    </div>
                    <div class="col-md-2">
                      <label for="">Nickname</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_nickname">
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="">Email</label>
                      <input type="email" class="form-control" placeholder="" v-model="form.off_email">
                    </div>
                    <div class="col-md-3">
                      <label for="">Tel</label>
                      <input type="text" class="form-control" id="phone" placeholder="" v-model="form.off_tel">
                    </div>
                    <div class="col-md-3">
                      <label for="">Position</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_position">
                    </div>
                    <div class="col-md-3">
                      <label for="">Personal ID</label>
                      <input type="text" class="form-control" id="personalid" placeholder="" v-model="form.off_personal_id">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="">Birth date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" placeholder="" id="off_bdate" v-model="form.off_bdate">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Image avatar</label>
                        <input type="file" id="exampleInputFile">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label for="off_bank_acc_name">Bank account name</label>
                      <!-- <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_bank_acc_name" v-model="form.off_bank_acc_name">
                        <option selected="selected" value="">--</option>
                        <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                      </select> -->
                      <input type="text" class="form-control" placeholder="" v-model="form.off_bank_acc_name">
                    </div>
                    <div class="col-md-3">
                      <label for="">Bank account number (number only)</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_bank_acc_number">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="">Work Start Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" placeholder="" id="off_work_start" v-model="form.off_start_working">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label for="">Work End Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" placeholder="" id="off_work_end" v-model="form.off_end_working">
                      </div>
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="">Username</label>
                      <input type="email" class="form-control" placeholder="" v-model="form.off_username" v-if="formType=='add'">
                      <input type="email" class="form-control" placeholder="" v-model="form.off_username" v-else readonly>
                    </div>
                    <!-- <div class="col-md-3">
                      <label for="">Password</label>
                      <input type="password" class="form-control" placeholder="" v-model="form.off_password">
                    </div> -->
                    
 
                    <div class="col-md-3" style="margin-top: 4px;">
                      <label>Permission</label>
                      <div class="form-group">
                        <input type="radio" id="rdo_status1" value="0" checked="" v-model="form.off_permission_level">
                        <label class="radio-label" for="rdo_status1">Officer</label>
                        <input type="radio" id="rdo_status2" value="1" v-model="form.off_permission_level" style="margin-left: 12px;">
                        <label class="radio-label" for="rdo_status2">Administrator</label>
                      </div>
                    </div>

                    <div class="col-md-6" style="margin-top: 4px;" v-if="formType == 'edit'">
                      <label>Status</label>
                      <div class="form-group">
                        <input type="radio" id="rdo_status3" value="1" checked="" v-model="form.off_status">
                        <label class="radio-label" for="rdo_status1">Active</label>
                        <input type="radio" id="rdo_status4" value="0" v-model="form.off_status" style="margin-left: 12px;">
                        <label class="radio-label" for="rdo_status2">Resign</label>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label for="off_bank_acc_name">Git Account Name</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_github_user">
                    </div>
                    <div class="col-md-3">
                      <label for="">Slack Account Name</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_slack_user">
                    </div>
                    <div class="col-md-3">
                      <label for="">Trello Account Name</label>
                      <input type="text" class="form-control" placeholder="" v-model="form.off_trello_user">
                    </div>
                  </div>
                </div>
                
          
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" @click="_validate()">Submit</button>
              <button type="submit" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function() {
      
      $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        endDate: Date()
      });
      $("#phone").inputmask({
        mask: '999 999 9999',
        placeholder: ' ',
        showMaskOnHover: false,
        showMaskOnFocus: false,
        onBeforePaste: function (pastedValue, opts) {
          let processedValue = pastedValue;
          return processedValue;
        },
        "oncomplete": function(e){
          main.form.off_tel = e.target.value.replace(/ /g, "");
        }
      });
      $("#personalid").inputmask({
        mask: '9 9999 99999 99 9',
        placeholder: ' ',
        showMaskOnHover: false,
        showMaskOnFocus: false,
        onBeforePaste: function (pastedValue, opts) {
          let processedValue = pastedValue;
          return processedValue;
        },
        "oncomplete": function(e){
          main.form.off_personal_id = e.target.value.replace(/ /g, "");
        }
      });
    });
    
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        officer_types: [],
        admin_id: '{{ $profile->off_id }}',
        off_id: '{{ $id }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 30
        },
        form: {
          off_code: '',
          off_type_id: '',
          off_prefixname: '',
          off_firstname: '',
          off_lastname: '',
          off_nickname: '',
          off_image: '',
          off_personal_id: '',
          off_bdate: '',
          off_tel: '',
          off_email: '',
          off_start_working: '',
          off_end_working: '',
          off_position: '',
          off_bank_acc_name: '',
          off_bank_acc_number: '',
          off_username: '',
          off_password: '123456',
          off_github_user: '',
          off_slack_user: '',
          off_trello_user: '',
          off_permission_level: 0,
          off_status: '',
        },
        formType: '{{ $formType }}'
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_officer_type();
          if (this.formType == 'edit') {
            this._get_officer_detail(true);
          }
          
          $(document).ready(function() {
            $('#off_bdate').on('changeDate', function() {
              main.form.off_bdate = $('#off_bdate').datepicker('getFormattedDate');
            });
            $('#off_work_start').on('changeDate', function() {
              main.form.off_start_working = $('#off_work_start').datepicker('getFormattedDate');
            });
            $('#off_work_end').on('changeDate', function() {
              main.form.off_end_working = $('#off_work_end').datepicker('getFormattedDate');
            });
          });
        })
      },
      methods: {
        _get_officer_type: function () {
          _nextOpenLoading();
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          axios.post('{{url('/api/admin/officertype')}}', _data).then(response => {
            _nextCloseLoading();
            let body = response.data;
            // console.log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.officer_types = result.filter((data) => {
                if (data.off_type_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _get_officer_detail: function (load) {
          if (load) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.admin_id,
            off_id: this.off_id

          };
          axios.post('{{url('/api/admin/officer/detail')}}', _data).then(response => {
            let body = response.data;
            // console.log("detail", body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              this.form = body.data;
            }
            else {
              swal({
                text: 'ทำรายการไม่สำเร็จ: ' + body.message,
                icon: 'warning'
              });
            }
          }).catch(error => {
            _nextCloseLoading();
            swal({
              text: error.message,
              icon: 'error'
            });
          });
        },
        _detail: function (id) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        },
        _validate: function() {
          if (this.form.off_type_id == '') return _nextAlert('กรุณาระบุ Type', 'war', 'Require Officer Type');
          if (this.form.off_prefixname == '') return _nextAlert('กรุณาระบุ Prefix', 'war', 'Require Officer Prefix');
          if (this.form.off_firstname.trim() == '') return _nextAlert('กรุณาระบุ Firstname', 'war', 'Require Firstname');
          if (this.form.off_lastname.trim() == '') return _nextAlert('กรุณาระบุ Lastname', 'war', 'Require Lastname');
          if (this.form.off_email.trim() == '') return _nextAlert('กรุณาระบุ Email', 'war', 'Require Email');
          if (!_checkEmailFormat(this.form.off_email.trim())) return _nextAlert('รูปแบบอีเมล์ผิด', 'war', 'Invalid Email');
          if (this.form.off_tel.trim() == '') return _nextAlert('กรุณาระบุ Tel', 'war', 'Require Tel');
          if (this.form.off_personal_id.trim() == '') return _nextAlert('กรุณาระบุ Personal ID', 'war', 'Require Personal ID');
          if (this.form.off_start_working.trim() == '') return _nextAlert('กรุณาระบุ Work Start Date', 'war', 'Require Work Start Date');
          if (this.form.off_username.trim() == '') return _nextAlert('กรุณาระบุ Username', 'war', 'Require Username');

          this._submit();
        },
        _submit: function() {
          // console.log("form", this.form);
          if (this.formType == 'add') {
            this._add_officer(true);
          }
          if (this.formType == 'edit') {
            this._update_officer(true);
          }
        },
        _add_officer: function (load) {
          if (load) {
            _nextOpenLoading();
          }
          let _data = this.form;
          _data.admin_id = this.admin_id;

          delete _data.off_image;
          delete _data.off_trello_user;

          // console.log("add form", _data);

          axios.post('{{url('/api/admin/officer/insert')}}', _data).then(response => {
            let body = response.data;
            //console.log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              swal({
                text: 'ทำรายการสำเร็จ: ',
                icon: 'success'
              }).then((result) => {
                _nextRedirectBack();
              });
              this._clear();
              
            }
            else {
              swal({
                text: 'ทำรายการไม่สำเร็จ: ' + body.message,
                icon: 'warning'
              });
            }
          }).catch(error => {
            _nextCloseLoading();
            swal({
              text: error.message,
              icon: 'error'
            });
          });
        },
        _update_officer: function (load) {
          if (load) {
            _nextOpenLoading();
          }
          let _data = this.form;
          _data.admin_id = this.admin_id;

          delete _data.off_image;
          delete _data.off_trello_user;

          // console.log("update form", _data);

          axios.post('{{url('/api/admin/officer/update')}}', _data).then(response => {
            _nextCloseLoading();
            let body = response.data;
            //console.log(body);
            if (body.status === 'ok') {
              swal({
                text: 'ทำรายการสำเร็จ: ',
                icon: 'success'
              }).then((result) => {
                _nextOpenLoading();
                _nextRedirectBack();
              });
              this._resetForm();
              
            }
            else {
              swal({
                text: 'ทำรายการไม่สำเร็จ: ' + body.message,
                icon: 'warning'
              });
            }
          }).catch(error => {
            _nextCloseLoading();
            swal({
              text: error.message,
              icon: 'error'
            });
          });
        },
        _clear: function() {
          if (this.formType == 'edit') {
            this._get_officer_detail(true);
          } else {
            this._resetForm();
          }
        },
        _resetForm: function() {
          let form = this.form;
          for (const key in form) {
            if (form.hasOwnProperty(key)) {
              form[key] = '';
            }
          }
          form.off_bdate = '';
          form.off_password = '123456';
          form.off_permission_level = 0;
        }
      }
    });
  </script>
@endsection