@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Note Leave Report
        <small>note leave of application DD officer</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('/admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('/admin/noteleave_report')}}">Note Leave Report</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title" style="line-height: 32px;">List :: Note Leave</h3>
                <button class="btn btn-success pull-right" id="exportXLSX" style="margin-left:5px;"><i class="fa fa-file-excel-o"></i> Export Excel</button>
                <div class="form-group pull-right col-md-5 margin-bottom-none" style="padding-right: 0;">
                    <select class="form-control select2" style="width: 100%" v-model="monthsSelected" tabindex="-1" aria-hidden="true" id="nl_month">
                    <option v-for="month in noteleave_months" :value="month.idkey">
                      @{{ month.value }}
                    </option>
                  </select>
                </div>
            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered" id="excelEXP">
                <thead>
                  <tr>
                    <th colspan="12" style="text-align: center">รายงานสรุปการลาประจำเดือน @{{ this.selectedMText }}</th>
                  </tr>
                  <tr>
                    <th rowspan="2" class="text-td-center" width="40">#</th>
                    <th rowspan="2" class="text-td-center" width="170">Officer Name</th>
                    <th v-bind:colspan="dimensionNoteLeave.length" style="text-align: center">ประเภทการลา</th>
                  </tr>
                  <tr>
                    <th v-for="data in dimensionNoteLeave" style="text-align: center">
                      @{{ data.nl_type_name }}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="(data, index) in officer_list">
                    <td class="text-td-center">
                      @{{ index+1 }}
                    </td>
                    <td class="text-td-left">
                      @{{ data.off_prefixname }}@{{ data.off_firstname }} (@{{ data.off_nickname }})
                    </td>
                    <td v-for="data2 in dimensionNoteLeave" style="text-align: center">
                      <div v-for="leaveData in lists">
                        <div v-if="leaveData.officer_data.off_id == data.off_id">
                          <div v-if="leaveData.nl_type_id == data2.nl_type_id">
                            <div class="text-td-left" v-if="leaveData.nl_leave_start != leaveData.nl_leave_end">
                              <p>&nbsp;@{{ _momentDateSuperShort(leaveData.nl_leave_start) }} - @{{ _momentDateSuperShort(leaveData.nl_leave_end) }} (จำนวน @{{ _momentDiffDateWork(leaveData.nl_leave_end, leaveData.nl_leave_start) }} วัน) </p>
                              <p>&nbsp;@{{ leaveData.nl_note }}&nbsp;/</p>
                            </div>
                            <div class="text-td-left" v-else>
                              <p v-if="_momentDiffTimeWork(leaveData.nl_time_start, leaveData.nl_time_end) < 8">&nbsp;@{{ _momentDateSuperShort(leaveData.nl_leave_start) }} (ครึ่งวัน (@{{ _momentDiffTimeWork(leaveData.nl_time_start, leaveData.nl_time_end) }} ชั่วโมง)) @{{ leaveData.nl_note }} &nbsp;/</p>
                              <p v-else-if="_momentDiffTimeWork(leaveData.nl_time_start, leaveData.nl_time_end) >= 8">&nbsp;@{{ _momentDateSuperShort(leaveData.nl_leave_start) }} (เต็มวัน (เศษ @{{ 8 - _momentDiffTimeWork(leaveData.nl_time_start, leaveData.nl_time_end)  }}) @{{ leaveData.nl_note }}&nbsp;/</p>
                              <p v-else>&nbsp;@{{ _momentDateSuperShort(leaveData.nl_leave_start) }} (เต็มวัน) @{{ leaveData.nl_note }}&nbsp;/</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box List -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
    $(function () {
      $('.select2').select2();
      
      $('#nl_month').change(function () {
        let val = $('#nl_month').val();
        if (val) {
          let ex = val.split("-");
          main._set_month_year(ex[1], ex[0]);
          main.monthsSelected = val;
          main.selectedMText = moment(ex[1]).format('MMMM') + ' ' + moment(ex[0]).format('YYYY');
        }
        main._get(true);
      });
      $('#exportXLSX').on('click',function(){
        let wb = XLSX.utils.table_to_book(document.getElementById('excelEXP'), {sheet:`${main.selectedMText}`});
        XLSX.writeFile(wb, `${main.selectedMText}.xls`);
      });
    });
    
    let main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        lists_request: [],
        officers: [],
        officer_list:[],
        noteleave_officers:[],
        noteleave_types: [],
        noteleave_months: [],
        off_id: '{{ $profile->off_id }}',
        nl_month: moment().format('M'),
        nl_year: moment().format('YYYY'),
        is_clear: false,
        monthsSelected: '',
        dimensionNoteLeave:[],
        selectedMText:'',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 100
        },
        data: {
          nl_id:'',
          off_id: '',
          nl_type_id: '',
          nl_time_type: '',
          nl_leave_start: moment().date(1).format('YYYY-MM-DD'),
          nl_leave_end: moment().endOf('month').add(1,'M').format('YYYY-MM-DD'),
          nl_time_start: '09:00',
          nl_time_end: '12:00',
          nl_note: '',
          nl_status: ''
        },
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_officer();
          this._get_noteleave_type();
          this._get_noteleave_months();
          this._get_noteleave_type_dimension();
          this._get(true);
        })
      },
      methods: {
        _set_month_year: function ($month, $year) {
          this.nl_month = $month;
          this.nl_year = $year;
        },
        _get_officer: function () {
          
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/admin/officer')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.officers = result.filter((data) => {
                if (data.off_status === 1) {
                  return data
                }
              });
              this.officer_list = result.filter((data) => {
                if (data.off_status === 1){
                  return data;
                }
              })
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },

        _get_noteleave_type_dimension: function(){
          axios.get('{{url('/api/admin/noteleave/type_dimension')}}').then(response => {
            let body = response.data;
            let result = body.data;
            // _log(body);
            this.dimensionNoteLeave = result.filter((data) => {
              if(data.nl_type_status === 1){
                return data;
              }
            });
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get_noteleave_type: function () {
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/admin/noteleavetype')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.noteleave_types = result.filter((data) => {
                if (data.nl_type_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get_noteleave_months: function () {
          axios.post('{{url('/api/admin/noteleavemonth')}}').then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              this.noteleave_months = body.data;
              this.monthsSelected = this.noteleave_months.filter((data) => {
                if(data.selected === 'y') {
                  return data.idkey;
                }
              })[0].idkey;
              let ex = this.monthsSelected.split("-");
              this.selectedMText = moment(ex[1]).format('MMMM') + ' ' + moment(ex[0]).format('YYYY');
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
            nl_month: this.nl_month,
            nl_year: this.nl_year,
          };
          
          // _log(_data);
          axios.post('{{url('/api/admin/noteleave/report')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;
              this.lists = result.filter((data) => {
                if (data.nl_status === 3 || data.nl_status === -1) {
                  return data
                }
              });
              this.lists_request = result.filter((data) => {
                if (data.nl_status === 1 || data.nl_status === 2) {
                  return data
                }
              });
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        }
      }
    });
  </script>
@endsection