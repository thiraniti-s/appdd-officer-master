@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Officer
        <small>list of officer in application DD</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> Home</li>
        <li class="active"><a href="{{url('/admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('/admin/officer')}}">Officer</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
        
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">List :: Officer</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-add-form" style="margin-right: .5em" @click="_addForm">เพิ่มรายชื่อ</i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                  <tr>
                    <th class="text-td-center" width="40">#</th>
                    <th class="text-td-center">Name</th>
                    <th class="text-td-center">Nickname</th>
                    <th class="text-td-center">Tel</th>
                    <th class="text-td-center">Email</th>
                    <th class="text-td-center">Github</th>
                    <th class="text-td-center">StartWork</th>
                    <th class="text-td-center">Position</th>
                    <th class="text-td-center" width="80">Status</th>
                    <th class="text-td-center" width="60">View</th>
                    <th class="text-td-center" width="80">Gen Acc.</th>
                  </tr>
                  <tr v-for="(data, index) in lists" >
                    <td class="text-td-center" v-if="data.off_status === 1">
                      @{{ rowNumber(index, params.page, params.limit) }}
                    </td>
                    <td class="text-td-center bg-resign" v-else>
                      @{{ rowNumber(index, params.page, params.limit) }}
                    </td>
                    <td class="text-td-left" v-if="data.off_status === 1">
                      @{{ data.off_prefixname }}@{{ data.off_firstname }} @{{ data.off_lastname }}
                    </td>
                    <td class="text-td-left bg-resign" v-else>
                      @{{ data.off_prefixname }}@{{ data.off_firstname }} @{{ data.off_lastname }}
                    </td>
                    <td class="text-td-center" v-if="data.off_status === 1">
                      @{{ data.off_nickname }}
                    </td>
                    <td class="text-td-center bg-resign" v-else>
                      @{{ data.off_nickname }}
                    </td>
                    <td class="text-td-center" v-if="data.off_status === 1">
                      @{{ data.off_tel }}
                    </td>
                    <td class="text-td-center bg-resign" v-else>
                      @{{ data.off_tel }}
                    </td>
                    <td class="text-td-left" v-if="data.off_status === 1">
                      @{{ data.off_email }}
                    </td>
                    <td class="text-td-left bg-resign" v-else>
                      @{{ data.off_email }}
                    </td>
                    <td class="text-td-left" v-if="data.off_status === 1">
                      @{{ data.off_github_user }}
                    </td>
                    <td class="text-td-left bg-resign" v-else>
                      &nbsp;
                    </td>
                    <td class="text-td-center" v-if="data.off_status === 1">
                      @{{ _momentDate(data.off_start_working, 'long') }}
                    </td>
                    <td class="text-td-center bg-resign" v-else>
                      @{{ _momentDate(data.off_start_working, 'long') }}
                    </td>
                    <td class="text-td-left" v-if="data.off_status === 1">
                      @{{ data.off_position }}
                    </td>
                    <td class="text-td-left bg-resign" v-else>
                      @{{ data.off_position }}
                    </td>
                    <td class="text-td-center" v-if="data.off_status === 1">
                      <span class="badge bg-green">Active</span>
                    </td>
                    <td class="text-td-center bg-resign" v-else>
                      <span class="badge bg-red">Resign</span>
                    </td>
                    <td class="text-td-center">
                      <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data.off_id)">
                        <i class="fa fa-search"></i>
                      </button>
                    </td>
                    <td class="text-td-center">
                      <button type="button" class="btn btn-block btn-xs btn-warning" @click="_gen_acc(data.off_id)"
                       v-if="data.off_status === 1">
                        <i class="fa fa-universal-access"></i>
                      </button>
                      <button type="button" class="btn btn-block btn-xs btn-warning" v-else disabled>
                        <i class="fa fa-universal-access"></i>
                      </button>
                    </td>
                  </tr>
                  <tr v-if="lists.length === 0">
                    <td colspan="10" class="empty-data">
                      <h4>-- Data Not Found --</h4>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="box-footer" v-if="master.total_pages > 1">
              <ul class="pagination pull-right">
                <li class="page-item mb-3" v-for="page in pagination">
                <a class="page-link" v-if="page == 'n'">...</a>
                  <a class="page-link active" v-else-if="page == params.page">@{{ page }}</a>
                  <a class="page-link" v-else @click="goto_page(page)">@{{ page }}</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        

        <!-- Box Add -->
        {{-- <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Officer</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="off_type_id">Type</label>
                      <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_type_id">
                        <option selected="selected" value="">--</option>
                        <option v-for="off_type in officer_types" :value="off_type.off_type_id">
                          @{{ off_type.off_type_name }}
                        </option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label for="">Officer Code</label>
                      <input type="text" class="form-control" placeholder="APPDD-0001" v-model="data.off_code">
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label for="off_prefixname">Prefix</label>
                      <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_prefixname">
                        <option selected="selected" value="">--</option>
                        <option value="นาย">นาย</option>
                        <option value="นาง">นาง</option>
                        <option value="นางสาว">นางสาว</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="">Firstname</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_firstname">
                    </div>
                    <div class="col-md-4">
                      <label for="">Lastname</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_lastname">
                    </div>
                    <div class="col-md-2">
                      <label for="">Nickname</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_nickname">
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="">Email</label>
                      <input type="email" class="form-control" placeholder="" v-model="data.off_email">
                    </div>
                    <div class="col-md-6">
                      <label for="">Tel</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_tel">
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="">Username</label>
                      <input type="email" class="form-control" placeholder="" v-model="data.off_username">
                    </div>
                    <div class="col-md-6">
                      <label for="">Password</label>
                      <input type="password" class="form-control" placeholder="" v-model="data.off_password">
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <label>Permission</label>
                  <div class="row">
                    <div class="col-md-12" style="margin-top: 4px;">
                      <input type="radio" id="rdo_status1" value="0" checked="" v-model="data.off_permission_level">
                      <label class="radio-label" for="rdo_status1">Officer</label>
                      <input type="radio" id="rdo_status2" value="1" v-model="data.off_permission_level" style="margin-left: 12px;">
                      <label class="radio-label" for="rdo_status2">Administrator</label>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="">Position</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_position">
                    </div>
                    <div class="col-md-6">
                      <label for="">Personal ID</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_personal_id">
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="">Birth date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" placeholder="" id="nl_leave_start" v-model="data.off_bdate">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Image avatar</label>
                        <input type="file" id="exampleInputFile">
                      </div>
                    </div>
                  </div>
                </div>
  
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="off_bank_acc_name">Bank account name</label>
                      <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_bank_acc_name">
                        <option selected="selected" value="">--</option>
                        <option value="นาย">ธนาคารกสิกรไทย</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label for="">Bank account number (number only)</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.off_bank_acc_number">
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" @click="_submit()">Submit</button>
              <button type="submit" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div> --}}
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
      });
    });
    
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        officer_types: [],
        off_id: '{{ $profile->off_id }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 20
        },
        pagination: [],
        data: {
          off_code: '',
          off_type_id: '',
          off_prefixname: '',
          off_firstname: '',
          off_lastname: '',
          off_nickname: '',
          off_image: '',
          off_personal_id: '',
          off_bdate: '1987-10-18',
          off_tel: '',
          off_email: '',
          off_start_working: '',
          off_end_working: '',
          off_position: '',
          off_bank_acc_name: '',
          off_bank_acc_number: '',
          off_username: '',
          off_password: '123456',
          off_github_user: '',
          off_permission_level: 0,
          off_status: '',
        }
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_officer_type();
          this._get(true);
        })
      },
      methods: {
        _get_officer_type: function () {
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          axios.post('{{url('/api/admin/officertype')}}', _data).then(response => {
            let body = response.data;
            // console.log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.officer_types = result.filter((data) => {
                if (data.off_type_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get: function (load) {
          if (load) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          axios.post('{{url('/api/admin/officer')}}', _data).then(response => {
            let body = response.data;
            // console.log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              this.lists = body.data.result;
              this._paging(this.params.page, body.data.total_pages);
            }
            else {
              swal({
                text: 'ทำรายการไม่สำเร็จ: ' + body.message,
                icon: 'warning'
              });
            }
          }).catch(error => {
            _nextCloseLoading();
            swal({
              text: error.message,
              icon: 'error'
            });
          });
        },
        _detail: function (id) {
          // _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
          _nextRedirect('{{url('/admin/officer/edit')}}'+'/'+id);
        },
        _gen_acc: function (id) {
          swal({
            title: 'ยืนยันการทำรายการ',
            text: 'ต้องการ Generate Account นี้ โดยการสุ่มรหัสผ่านและส่งไปให้ผู้ใช้งานทางอีเมล์ ใช่หรือไม่ ?',
            icon: 'info',
            buttons: true,
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                off_id: id
              };
              // _log(_data);
              axios.post('{{url('/api/admin/officer/genaccount')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                  
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
          
        },
        rowNumber(index, page_number, page_size) {
          return (index + 1) + ((page_number - 1) * page_size)
        },
        _paging (current, last, delta = 2) {
            let left = (current + 1) - delta,
                right = current + delta + 1,
                range = [];

            if (right > last) {
                right = last;
                left = last - delta * 2;
                left = left < 1 ? 1 : left;
            }

            if (left <= 1) {
                left = 1;
                right = Math.min(delta * 2 + 1, last);
            }

            if (left <= 3) {
                for (let i = 1; i < left; i++) {
                    range.push(i);
                }
            } else {
                range.push(1);
                range.push("n");
            }

            for (let i = left; i <= right; i++) {
                range.push(i);
            }

            if (right >= last - 2) {
                for (let i = right + 1; i <= last; i++) {
                    range.push(i);
                }
            } else {
                range.push("n");
                range.push(last);
            }

            this.pagination = range;
        },
        goto_page: function (page) {
          this.params.page = page;
          this._get(true);
        },
        _addForm: function() {
          _nextRedirect('/admin/officer/add/0');
        }
      },
    });
  </script>
@endsection