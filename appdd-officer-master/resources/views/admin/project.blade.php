@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Project
        <small>Project of application DD</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('admin/project')}}">Project</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
      
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">List :: Project</h3>
            </div>
          
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center">Name</th>
                  <th class="text-td-center">Description</th>
                  <th class="text-td-center">Application</th>
                  <th class="text-td-center">Skill</th>
                  <th class="text-td-center">Tools</th>
                  <th class="text-td-center" width="130">Start</th>
                  <th class="text-td-center" width="80">Status</th>
                  <th class="text-td-center" width="60">View</th>
                  <th class="text-td-center" width="60">Edit</th>
                  <th class="text-td-center" width="60">Delete</th>
                </tr>
                <tr v-for="(data, index) in lists">
                  <td class="text-td-center">
                    @{{ (index + 1) + ((params.page - 1) * params.limit) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.pro_name }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.pro_description }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.pro_application }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.pro_skill }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.pro_tools }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDate(data.pro_start, 'long') }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-yellow" v-if="data.pro_status==1">Progress</span>
                    <span class="badge bg-blue" v-else-if="data.pro_status==2">Maintenance</span>
                    <span class="badge bg-green" v-else-if="data.pro_status==3">Finished</span>
                    <span class="badge bg-gray" v-else>Cancel</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-warning" @click="_edit(data)">
                      <i class="fa fa-pencil"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_delete(data)">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                <tr v-if="lists.length === 0">
                  <td colspan="9" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
  
              <!-- Pagination -->
              <div class="col-sm-12" v-if="master.total_pages > 1">
                <div class="data-table-paginate text-td-center">
                  <ul class="pagination">
                    <li class="" v-for="page in master.total_pages">
                      <span class="active" v-if="page === params.page" @click="_goto_page(page)">@{{ page }}</span>
                      <span class="paginate_button" v-else @click="_goto_page(page)">@{{ page }}</span>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- #Pagination -->
          
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Project</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" v-model="data.pro_name">
                </div>
                <div class="form-group">
                  <label for="pro_application">Application</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="pro_application">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option value="Web Application">Web Application</option>
                    <option value="Mobile Application">Mobile Application</option>
                    <option value="Web & Mobile Application">Web & Mobile Application</option>
                    <option value="Window Application">Window Application</option>
                    <option value="MacOS Application">MacOS Application</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="#">Date Start</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control datepicker" id="pro_start" value="<?php echo date('Y-m-d'); ?>" readonly>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Description</label>
                  <input type="text" class="form-control" v-model="data.pro_description">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Skill</label>
                      <input type="text" class="form-control" v-model="data.pro_skill">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tools</label>
                      <input type="text" class="form-control" v-model="data.pro_tools">
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Status</label>
                  <div class="row">
                    <div class="col-md-12" style="margin-top: 4px;">
                      <input type="radio" id="rdo_status1" value="1" v-model="data.pro_status">
                      <label class="radio-label" for="rdo_status1">Progress</label>
                      <input type="radio" id="rdo_status2" value="2" v-model="data.pro_status" style="margin-left: 12px;">
                      <label class="radio-label" for="rdo_status2">Maintenance</label>
                      <input type="radio" id="rdo_status3" value="3" v-model="data.pro_status" style="margin-left: 12px;">
                      <label class="radio-label" for="rdo_status3">Finished</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer" id="demo2">
              <button type="button" class="btn btn-primary" @click="_submit()">Submit</button>
              <button type="button" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
      });
    });
    
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        off_id: '{{ $profile->off_id }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 30
        },
        data: {
          pro_id: '',
          pro_name: '',
          pro_description: '',
          pro_application: '',
          pro_skill: '',
          pro_tools: '',
          pro_start: moment().format('YYYY-MM-DD'),
          pro_status: 1
        }
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get(true);
        })
      },
      methods: {
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          // _log(_data);
          axios.post('{{url('/api/admin/project')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              this.lists = body.data.result;
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _goto_page:function (page) {
          this.params.page = page;
          this._get(true);
        },
        _clear: function () {
          $('.select2').val('').trigger('change');
          this.data.pro_id = '';
          this.data.pro_name = '';
          this.data.pro_description = '';
          this.data.pro_application = '';
          this.data.pro_skill = '';
          this.data.pro_tools = '';
          this.data.pro_start = moment().format('YYYY-MM-DD');
          this.data.pro_status = 1;
          $('#pro_start').val(moment().format('YYYY-MM-DD'));
        },
        _validate: function () {
          this.data.pro_application = $('#pro_application').val();
          this.data.pro_start = $('#pro_start').val();
          
          if (!_nextIsNotValid(this.data.pro_name, '', 'Please input name')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.pro_description, '', 'Please input description')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.pro_application, '', 'Please choose application')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.pro_skill, '', 'Please input skill')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.pro_tools, '', 'Please input tools')) {
            return false;
          }
          
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการบันทึก',
              text: 'ต้องการบันทึกข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                // add new
                if (this.data.pro_id === '') {
                  let _data = {
                    admin_id: this.off_id,
                    pro_name: this.data.pro_name,
                    pro_description: this.data.pro_description,
                    pro_application: this.data.pro_application,
                    pro_skill: this.data.pro_skill,
                    pro_tools: this.data.pro_tools,
                    pro_start: this.data.pro_start,
                    pro_status: parseInt(this.data.pro_status)
                  };
                  // _log(_data);
                  axios.post('{{url('/api/admin/project/insert')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success',
                        closeOnClickOutside: false
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    }
                    else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
                // update data
                else {
                  let _data = {
                    admin_id: this.off_id,
                    pro_id: this.data.pro_id,
                    pro_name: this.data.pro_name,
                    pro_description: this.data.pro_description,
                    pro_application: this.data.pro_application,
                    pro_skill: this.data.pro_skill,
                    pro_tools: this.data.pro_tools,
                    pro_start: this.data.pro_start,
                    pro_status: parseInt(this.data.pro_status)
                  };
                  // _log(_data);
                  axios.post('{{url('/api/admin/project/update')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success',
                        closeOnClickOutside: false
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    }
                    else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
              }
            });
          }
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        },
        _edit: function (data) {
          this.data.pro_id = data.pro_id;
          this.data.pro_name = data.pro_name;
          this.data.pro_description = data.pro_description;
          this.data.pro_application = data.pro_application;
          $('#pro_application').val(data.pro_application).trigger('change');
          this.data.pro_skill = data.pro_skill;
          this.data.pro_tools = data.pro_tools;
          this.data.pro_start = moment(data.pro_start).format('YYYY-MM-DD');
          $('#pro_start').val(moment(data.pro_start).format('YYYY-MM-DD'));
          this.data.pro_status = data.pro_status;
        },
        _delete: function (data) {
          swal({
            title: 'ยืนยันการลบ',
            text: 'ต้องการลบข้อมูล \"' + data.pro_name + '\" หรือไม่ ?',
            icon: 'info',
            buttons: ['Cancel', 'Confirm']
          }).then(confirm => {
            if (confirm) {
              let _data = {
                admin_id: this.off_id,
                pro_id: data.pro_id,
                pro_name: data.pro_name,
                pro_description: data.pro_description,
                pro_application: data.pro_application,
                pro_skill: data.pro_skill,
                pro_tools: data.pro_tools,
                pro_start: data.pro_start,
                pro_status: 0
              };
              // _log(_data);
              axios.post('{{url('/api/admin/project/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
      }
    });
  </script>
@endsection