@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Officer Type
        <small>Master data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{url('/admin/master')}}">Master Data</a></li>
        <li class="active"><a href="{{url('/admin/master/officertype')}}">Officer Type</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
        
        <div class="col-xs-12">
          <div class="box box-danger ">
            <div class="box-header">
              <h3 class="box-title">List :: Officer Type</h3>
            </div>
          
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
          
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center">Name</th>
                  <th class="text-td-center" width="100">Status</th>
                  <th class="text-td-center" width="180">Last Update</th>
                  <th class="text-td-center" width="60">Edit</th>
                  <th class="text-td-center" width="60">Delete</th>
                </tr>
                <tr v-for="(data, index) in lists">
                  <td class="text-td-center">
                    @{{ (index + 1) + ((params.page - 1) * params.limit) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.off_type_name }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-green" v-if="data.off_type_status === 1">Active</span>
                    <span class="badge bg-gray" v-else>Unactive</span>
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDateTime(data.updated_at) }}
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-warning" @click="_edit(data)">
                      <i class="fa fa-pencil"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_delete(data)">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
                <tr v-if="lists.length === 0">
                  <td colspan="6" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
              
              <!-- Pagination -->
              <div class="col-sm-12" v-if="master.total_pages > 1">
                <div class="data-table-paginate text-td-center">
                  <ul class="pagination">
                    <li class="" v-for="page in master.total_pages">
                      <span class="active" v-if="page === params.page" @click="_goto_page(page)">@{{ page }}</span>
                      <span class="paginate_button" v-else @click="_goto_page(page)">@{{ page }}</span>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- #Pagination -->
              
            </div>
          </div>
        </div>
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Officer Type</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" placeholder="" v-model="data.off_type_name" id="typeleave">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Status</label>
                  <div class="row">
                    <div class="col-md-12" style="margin-top: 4px;">
                      <input type="radio" id="rdo_status1" value="1" checked="" v-model="data.off_type_status">
                      <label class="radio-label" for="rdo_status1">Active</label>
                      <input type="radio" id="rdo_status2" value="0" v-model="data.off_type_status" style="margin-left: 12px;">
                      <label class="radio-label" for="rdo_status2">Unactive</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer" id="demo2">
              <button type="submit" class="btn btn-primary" @click="_submit()">Submit</button>
              <button type="submit" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        off_id: '{{ $profile->off_id }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 10
        },
        data: {
          off_type_id: '',
          off_type_name: '',
          off_type_status: 1
        }
      },
      mounted: function () {
        this.$nextTick(function () {        
          this._get(true);
        })
      },
      methods: {
        _get: function (loading = false) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          axios.post('{{url('/api/admin/officertype')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              this.lists = body.data.result;
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _goto_page:function (page) {
          this.params.page = page;
          this._get(true);
        },
        _clear: function () {
          this.data.off_type_id = '';
          this.data.off_type_name = '';
          this.data.off_type_status = 1;
        },
        _validate: function () {
          if (!_nextIsNotValid(this.data.off_type_name, '', 'Please input name')) {
            return false;
          }
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการบันทึก',
              text: 'ต้องการบันทึกข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                // add new
                if (this.data.off_type_id === '') {
                  let _data = {
                    admin_id: this.off_id,
                    off_type_name: this.data.off_type_name,
                    off_type_status: parseInt(this.data.off_type_status)
                  };
                  // _log(data);
                  axios.post('{{url('/api/admin/officertype/insert')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success'
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    }
                    else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
                // update data
                else {
                  let _data = {
                    admin_id: this.off_id,
                    off_type_id: this.data.off_type_id,
                    off_type_name: this.data.off_type_name,
                    off_type_status: parseInt(this.data.off_type_status)
                  };
                  // _log(_data);
                  axios.post('{{url('/api/admin/officertype/update')}}', _data).then(response => {
                    let body = response.data;
                    // _log(body);
                    _nextCloseLoading();
                    if (body.status === 'ok') {
                      swal({
                        text: 'ทำรายการสำเร็จ',
                        icon: 'success'
                      }).then(ok => {
                        this._clear();
                        this._get(true);
                      });
                    }
                    else {
                      _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                    }
                  }).catch(error => {
                    _nextCloseLoading();
                    _nextAlert(error.message, 'err');
                  });
                }
              }
            });
          }
        },
        _edit: function (data) {
          this.data.off_type_id = data.off_type_id;
          this.data.off_type_name = data.off_type_name;
          this.data.off_type_status = data.off_type_status;
        },
        _delete: function (data) {
          swal({
            title: 'ยืนยันการลบ',
            text: 'ต้องการลบข้อมูล \"' + data.off_type_name + '\" หรือไม่ ?',
            icon: 'info',
            buttons: ['Cancel', 'Confirm']
          }).then(confirm => {
            if (confirm) {
              let _data = {
                admin_id: this.off_id,
                off_type_id: data.off_type_id,
                off_type_name: data.off_type_name,
                off_type_status: -1
              };
              // _log(_data);
              axios.post('{{url('/api/admin/officertype/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success'
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
      }
    });
  </script>

@endsection