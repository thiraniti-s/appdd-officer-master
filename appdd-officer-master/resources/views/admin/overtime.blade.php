@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Over Time
        <small>over time of application DD officer</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('admin')}}">Administrator</a></li>
        <li class="active"><a href="{{url('admin/overtime')}}">Over Time</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <!-- Box Request -->
        <div class="col-xs-12" v-if="lists_request.length > 0">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Request :: Over Time</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="170">Officer Name</th>
                  <th class="text-td-center" width="120">Date</th>
                  <th class="text-td-center" width="150">Time</th>
                  <th class="text-td-center">Note / Comment</th>
                  <th class="text-td-center" width="170">Reference</th>
                  <th class="text-td-center" width="80">Status</th>
                  <th class="text-td-center" width="60">View</th>
                  <th class="text-td-center" width="180">Action</th>
                </tr>
                <tr v-for="(data, index) in lists_request">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.officer_data.off_prefixname }}@{{ data.officer_data.off_firstname }} (@{{ data.officer_data.off_nickname }})
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDate(data.ot_date) }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentTime(data.ot_time_start) }} - @{{ _momentTime(data.ot_time_end) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.ot_note }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.ot_reference }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-gray" v-if="data.ot_status==1">Waiting</span>
                    <span class="badge bg-yellow" v-else-if="data.ot_status==2">InReview</span>
                    <span class="badge bg-gray" v-else>Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center" v-if="data.ot_status!==3">
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-warning" @click="_review(data)" v-if="data.ot_status==1">
                        <i class="fa fa-circle-o"></i> RW
                      </button>
                    </div>
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-success" @click="_approve(data)" v-if="data.ot_status==2 || data.ot_status==1">
                        <i class="fa fa-check"></i> AP
                      </button>
                    </div>
                    <div class="col-md-4 pd0-pd3">
                      <button type="button" class="btn btn-block btn-xs btn-danger" @click="_reject(data)" v-if="data.ot_status==2 || data.ot_status==1">
                        <i class="fa fa-times"></i> RJ
                      </button>
                    </div>
                  </td>
                  <td class="text-td-center" v-else>
                    -
                  </td>
                </tr>
                <tr v-if="lists_request.length == 0">
                  <td colspan="9" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box Request -->
  
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title" style="line-height: 32px;">List :: Over Time</h3>
              <div class="form-group pull-right col-md-4 margin-bottom-none" style="padding-right: 0;">
                <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="ot_month">
                  <option v-for="month in overtime_months" :value="month.idkey">
                    @{{ month.value }}
                  </option>
                </select>
              </div>
            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="170">Officer Name</th>
                  <th class="text-td-center" width="120">Date</th>
                  <th class="text-td-center" width="150">Time</th>
                  <th class="text-td-center" width="100">CalHour</th>
                  <th class="text-td-center">Note / Comment</th>
                  <th class="text-td-center" width="170">Reference</th>
                  <th class="text-td-center" width="80">Status</th>
                  <th class="text-td-center" width="60">View</th>
                </tr>
                <tr v-for="(data, index) in lists">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.officer_data.off_prefixname }}@{{ data.officer_data.off_firstname }} (@{{ data.officer_data.off_nickname }})
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDate(data.ot_date) }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentTime(data.ot_time_start) }} - @{{ _momentTime(data.ot_time_end) }}
                  </td>
                  <td class="text-td-center">
                    <span v-if="data.ot_status==-1">---</span>
                    <span v-else>@{{ _momentDiffTimeWork(data.ot_time_start, data.ot_time_end) }}</span>
                  </td>
                  <td class="text-td-left" v-if="data.ot_status==-1">
                    @{{ data.ot_note }}<span class="text-red">@{{ !! data.ot_reject_comment ? ' ==> '+data.ot_reject_comment : '' }}</span>
                  </td>
                  <td class="text-td-left" v-else>
                    @{{ data.ot_note }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.ot_reference }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-green" v-if="data.ot_status==3">Approve</span>
                    <span class="badge bg-red" v-else-if="data.ot_status==-1">Reject</span>
                    <span class="badge bg-gray" v-else>Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                </tr>
                <tr v-if="lists.length == 0">
                  <td colspan="9" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form :: Over Time (OT)</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="officer">Officer</label>
                  <select class="form-control select2" style="width: 100%" tabindex="-1" aria-hidden="true" id="off_id">
                    <option selected="selected" value="">-- เลือก --</option>
                    <option v-for="off in officers" :value="off.off_id">
                      @{{ off.off_prefixname }}@{{ off.off_firstname }} @{{ off.off_lastname }} (@{{ off.off_nickname }})
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" id="ot_date" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Note</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.ot_note">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Reference</label>
                      <input type="text" class="form-control" placeholder="" v-model="data.ot_reference">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Time</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="ot_time_start" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="ot_time_end" readonly>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="button" class="btn btn-primary" @click="_submit()">Submit</button>
              <button type="button" class="btn btn-default" @click="_clear()">Clear</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
      });
      $('#ot_time_start').timepicker({
        defaultTime: '09:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false,
      });
      $('#ot_time_end').timepicker({
        defaultTime: '18:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false
      });
  
      $('#ot_month').change(function () {
        let val = $('#ot_month').val();
        if (val) {
          let ex = val.split("-");
          main._set_month_year(ex[1], ex[0]);
        }
        main._get(true);
      })
    });
    
    let main = new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        lists_request: [],
        officers: [],
        overtime_months: [],
        off_id: '{{ $profile->off_id }}',
        off_firstname: '{{ $profile->off_firstname }}',
        ot_month: moment().format('M'),
        ot_year: moment().format('YYYY'),
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 100
        },
        data: {
          off_id: '',
          ot_note: '',
          ot_reference: '',
          ot_date: moment().format('YYYY-MM-DD'),
          ot_time_start: '09:00',
          ot_time_end: '18:00'
        }
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get_officer();
          this._get_overtime_months();
          this._get(true);
        })
      },
      methods: {
        _set_month_year: function ($month, $year) {
          this.ot_month = $month;
          this.ot_year = $year;
        },
        _get_officer: function () {
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
          };
          axios.post('{{url('/api/admin/officer')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              let result = body.data.result;
              this.officers = result.filter((data) => {
                if (data.off_status === 1) {
                  return data
                }
              });
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get_overtime_months: function () {
          axios.post('{{url('/api/admin/overtimemonth')}}').then(response => {
            let body = response.data;
            // _log(body);
            if (body.status === 'ok') {
              this.overtime_months = body.data;
            }
          }).catch(error => {
            _nextAlert(error.message, 'err');
          });
        },
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            admin_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit,
            ot_month: this.ot_month,
            ot_year: this.ot_year,
          };
          _log(_data);
          axios.post('{{url('/api/admin/overtime')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;

              this.lists = result.filter((data) => {
                if (data.ot_status === 3 || data.ot_status === -1) {
                  return data
                }
              });
              this.lists_request = result.filter((data) => {
                if (data.ot_status === 1 || data.ot_status === 2) {
                  return data
                }
              });
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _clear: function () {
          // $('.select2').val('').trigger('change');
          $('#off_id').val('').trigger('change');
          this.data.ot_note = '';
          this.data.ot_reference = '';
          this.data.ot_date = moment().format('YYYY-MM-DD');
          this.data.ot_time_start = '09:00';
          this.data.ot_time_end = '18:00';
          $('#ot_date').val(moment().format('YYYY-MM-DD'));
          $('#ot_time_start').val('09:00');
          $('#ot_time_end').val('18:00');
        },
        _validate: function () {
          this.data.off_id = $('#off_id').val();
          this.data.ot_date = $('#ot_date').val();
          this.data.ot_time_start = $('#ot_time_start').val();
          this.data.ot_time_end = $('#ot_time_end').val();

          if (!_nextIsNotValid(this.data.off_id, '', 'Please select officer')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_note, '', 'Please input note')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_reference, '', 'Please input reference')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_date, '', 'Please choose date')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_time_start, '', 'Please choose time start')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_time_end, '', 'Please choose time end')) {
            return false;
          }
          if (!moment(this.data.ot_time_start, 'HH:ss').isBefore(moment(this.data.ot_time_end, 'HH:ss'))) {
            swal({
              text: 'Time is wrong, pls check again',
              icon: 'warning'
            });
            return false;
          }
          
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการบันทึก',
              text: 'ต้องการบันทึกข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                let data = {
                  admin_id: this.off_id,
                  off_id: this.data.off_id,
                  ot_note: this.data.ot_note,
                  ot_reference: this.data.ot_reference,
                  ot_date: this.data.ot_date,
                  ot_time_start: this.data.ot_time_start,
                  ot_time_end: this.data.ot_time_end
                };
                // _log(data);
                axios.post('{{url('/api/admin/overtime/insert')}}', data).then(response => {
                  let body = response.data;
                  // _log(body);
                  _nextCloseLoading();
                  if (body.status === 'ok') {
                    swal({
                      text: 'ทำรายการสำเร็จ',
                      icon: 'success',
                      closeOnClickOutside: false
                    }).then(ok => {
                      this._clear();
                      this._get(true);
                    });
                  }
                  else {
                    _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                  }
                }).catch(error => {
                  _nextCloseLoading();
                  _nextAlert(error.message, 'err');
                });
              }
            });
          }
        },
        _review: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบลาเป็น "In Review" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                ot_id: data.ot_id,
                ot_status: 2
              };
              axios.post('{{url('/api/admin/overtime/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _approve: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบลาเป็น "Approve" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                ot_id: data.ot_id,
                ot_status: 3
              };
              axios.post('{{url('/api/admin/overtime/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _reject: function (data) {
          swal({
            content: 'input',
            title: 'Reject Comment',
            text: 'Write reject comment here:',
            icon: 'warning',
            buttons: [true, 'Reject'],
            dangerMode: true,
            closeOnClickOutside: false
          }).then(message => {
            if (message) {
              _nextOpenLoading();
              let _data = {
                admin_id: this.off_id,
                ot_id: data.ot_id,
                ot_status: -1,
                ot_reject_comment: message
              };
              axios.post('{{url('/api/admin/overtime/status/update')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
            else {
              _nextAlert('ยกเลิกการทำรายการ', 'war');
            }
          });
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        }
      }
    });
  </script>
@endsection