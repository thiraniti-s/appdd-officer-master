@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        เปลี่ยนรหัสผ่าน
        <small>จัดการรหัสของคุณด้วยตัวเองง่ายๆ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/profile')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('changepassword')}}">เปลี่ยนรหัสผ่าน</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <!-- Box Edit -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ฟอร์มเปลี่ยนรหัสผ่าน</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">รหัสผ่านเดิม <span class="required">*</span></label>
                  <input type="text" class="form-control" placeholder="" v-model="data.old_password" maxlength="16">
                </div>
                <div class="form-group">
                  <label for="">รหัสผ่านใหม่ <span class="required">*</span></label>
                  <input type="password" class="form-control" placeholder="password at least 6 characters" v-model="data.new_password" maxlength="16">
                </div>
                <div class="form-group">
                  <label for="">ยืนยันรหัสผ่านใหม่ <span class="required">*</span></label>
                  <input type="password" class="form-control" placeholder="enter same new password" v-model="data.confirm_password" maxlength="16">
                </div>
              </div>
              <div class="col-md-8">
              
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" @click="_submit()">เปลี่ยนรหัสผ่าน</button>
              <button type="submit" class="btn btn-default" @click="_clear()">เคลียร์</button>
            </div>
          </div>
        </div>
        <!-- #Box Edit -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    new Vue({
      el: '#main',
      data: {
        master: {},
        off_id: '{{ $profile->off_id }}',
        data: {
          old_password: '',
          new_password: '',
          confirm_password: '',
        }
      },
      mounted: function () {
        this.$nextTick(function () {
        
        })
      },
      methods: {
        _clear: function () {
          this.data.old_password = '';
          this.data.new_password = '';
          this.data.confirm_password = '';
        },
        _validate: function () {
          if (!_nextIsNotValid(this.data.old_password, '', 'กรุณากรอกรหัสผ่านเดิม')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.new_password, '', 'กรุณากรอกรหัสผ่านใหม่')) {
            return false;
          }
          if (this.data.new_password.length < 6) {
            swal({
              text: 'รหัสผ่านต้องมีความยาว 6 ตัวขึ้นไป',
              icon: 'warning'
            });
            return false;
          }
          if (!_nextIsNotValid(this.data.confirm_password, '', 'กรุณากรอกยืนยันรหัสผ่านใหม่')) {
            return false;
          }
          if (this.data.new_password !== this.data.confirm_password) {
            swal({
              text: 'รหัสยืนยันไม่ถูกต้อง กรุณาตรวจสอบ',
              icon: 'warning'
            });
            return false;
          }
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการเปลี่ยนรหัสผ่าน',
              text: 'ต้องการเปลี่ยนรหัสผ่านตามข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                let _data = {
                  off_id: this.off_id,
                  old_password: this.data.old_password,
                  new_password: this.data.new_password
                };
                // _log(_data);
                axios.post('{{url('/api/officer/changepassword')}}', _data).then(response => {
                  let body = response.data;
                  // _log(body);
                  _nextCloseLoading();
                  if (body.status === 'ok') {
                    swal({
                      text: 'ทำรายการสำเร็จ',
                      icon: 'success'
                    }).then(ok => {
                      this._clear();
                    });
                  }
                  else {
                    _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                  }
                }).catch(error => {
                  _nextCloseLoading();
                  _nextAlert(error.message, 'err');
                });
              }
            });
          }
        },
      }
    });
  </script>
@endsection