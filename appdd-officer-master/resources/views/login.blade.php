<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <title>AppDD Officer | Login</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="{{asset('css/custom.css')}}">
  <link rel="stylesheet" href="{{asset('css/loader.css')}}">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <script src="{{asset('js/vue.min.js')}}"></script>
  <script src="{{asset('js/vue-resource@1.3.4')}}"></script>
  <script src="{{asset('js/axios.min.js')}}"></script>
  <script src="{{asset('/js/next.js')}}"></script>
  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
</head>
<body class="hold-transition login-page" style="overflow: hidden;">

@include('components.loading')

<div class="my-login-box" id="main" v-cloak>
  <div class="login-logo">
    <a href="{{url('/')}}"><b>AppDD</b>Officer</a>
  </div>
  <div class="login-box-body rd4">
    <p class="login-box-msg">Login to start your session</p>
      <div class="form-group has-feedback">
        <input type="text" id="username" class="form-control rd4" placeholder="Username" v-if="officer.off_username" v-model="officer.off_username">
        <input type="text" id="username" class="form-control rd4" placeholder="Username" v-else v-model="officer.off_username" autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="password" class="form-control rd4" placeholder="Password" v-if="officer.off_username" v-model="officer.password" autofocus>
        <input type="password" id="password" class="form-control rd4" placeholder="Password" v-else v-model="officer.password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4" style="margin-top: 4px;">
          <button class="btn btn-danger btn-block rd4" @click="login()">Login</button>
        </div>
      </div>
      <a class="cur-hand" @click="forgetpassword()">Forgot password?</a>
  </div>
</div>

<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/crypto-js.js')}}"></script>
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script>
  $(function() {
    $('#username').keypress(function (e) {
      e.keyCode === 13 ? main.login() : false
    });
    $('#password').keypress(function (e) {
      e.keyCode === 13 ? main.login() : false
    });
  });
  
  const main = new Vue({
    el: '#main',
    data: {
      master: {},
      officer: {
        off_username: '{{ $username }}',
        off_password: '',
        password: '',
      }
    },
    mounted: function () {
      this.$nextTick(function () {
      
      });
    },
    methods: {
      login: function () {
        this.officer.off_password = CryptoJS.MD5(this.officer.password).toString()
        _nextOpenLoading();
        axios.post('{{url('/api/login')}}', this.officer).then(response => {
          let body = response.data;
          // _log(body);
          _nextCloseLoading();
          if (body.status === 'ok') {
            _nextReload();
            // this.master = body.data;
            // let title_text = 'ยืนยันตัวตนเรียบร้อย';
            // let msg = 'ยินดีต้อนรับ คุณ'+this.master.off_firstname+' '+this.master.off_lastname;
            // swal({
            //   title: title_text,
            //   text: msg,
            //   icon: 'success',
            //   button: 'OK',
            //   closeOnClickOutside: false,
            //   closeOnEsc: false,
            // }).then(done => {
            //   _nextReload();
            // });
          }
          else {
            swal({
              title: 'Warning',
              text: body.message,
              icon: 'warning',
              button: 'OK',
              closeOnClickOutside: false,
              closeOnEsc: false,
            }).then(done => {
              $('#password').focus();
            });
          }
        }).catch(error => {
          _nextCloseLoading();
          _nextAlert(error.message, 'err');
          _log(error);
        });
      },
      forgetpassword: function () {
        _nextAlert('Please contact Support for resest password !!', 'inf');
      }
    }
  });
</script>

</body>
</html>