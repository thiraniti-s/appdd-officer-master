@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        Blank
        <small>subtitle</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> Home</li>
        <li class="active"><a href="{{url('/')}}">Blank</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
      
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    const main = new Vue({
      el: '#main',
      data: {
      
      },
      mounted: function () {
        this.$nextTick(function () {
        
        })
      },
      methods: {
      
      }
    });
  </script>
@endsection