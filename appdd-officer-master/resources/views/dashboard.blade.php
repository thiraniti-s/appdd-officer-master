@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        แดชบอร์ด
        <small>หน้าหลักของคุณ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('/dashboard')}}">Dashboard</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
        <div class="col-md-9">
          <div class="row">
            <!-- Box Announcement -->
            <div class="col-md-7">
              <div class="box box-warning direct-chat-success">
                <div class="box-header with-border">
                  <h3 class="box-title">ประกาศบริษัท&nbsp;&nbsp;<i class="fa fa-bullhorn text-warning"></i></h3>
                </div>
        
                <div class="box-body">
                  <div class="direct-chat-msg" v-for="data in announces">
                    <div class="direct-chat-info clearfix" style="margin-bottom: 6px;">
                      <span class="direct-chat-name pull-left" v-if="data.officer">K @{{ data.officer.off_firstname }} @{{ data.officer.off_lastname }}</span>
                      <span class="direct-chat-timestamp pull-right">@{{ _momentDate(data.ann_date) }}</span>
                    </div>
                    <img class="direct-chat-img" :src="data.officer.off_image" onerror="this.src='{{asset("dist/img/avatar_man.png")}}'" alt="message user image" v-if="data.officer.off_image">
                    <img class="direct-chat-img" src="{{asset("dist/img/avatar_man.png")}}" alt="message user image" v-else>
                    <div class="direct-chat-text">
                      @{{ data.ann_text }}
                    </div>
                  </div>
  
                  <div class="direct-chat-msg" v-if="announces.length == 0">
                    <div class="direct-chat-info clearfix" style="margin-bottom: 8px;">
                      <span class="direct-chat-name pull-left">Bot Officer</span>
                      <span class="direct-chat-timestamp pull-right">{{ Carbon\Carbon::now()->format('d F Y - H:i') }}</span>
                    </div>
                    <img class="direct-chat-img" src="{{asset("dist/img/avatar_man.png")}}" alt="message user image">
                    <div class="direct-chat-text">
                      ยังไม่มีประกาศใดๆ
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- #Box Announcement -->
  
            <!-- Box Chart Time Usage -->
            <div class="col-md-5">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">เวลาทำงาน&nbsp;&nbsp;<i class="fa fa-hourglass text-success"></i></h3>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="chart-responsive">
                        <canvas id="pieChart" height="310" width="336" style="width: 168px; height: 155px;"></canvas>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-green"></i> ทำงาน</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> ลางาน</li>
                        <li><i class="fa fa-circle-o text-blue"></i> ONSITE</li>
                        <li><i class="fa fa-circle-o text-red"></i> ล่วงเวลา</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="text-success">
                      <a>
                        <span class="text-success">เวลาทำงาน</span>
                        <span class="pull-right text-success">@{{ times.work }} ชม.</span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="text-yellow">เวลาลางาน</span>
                        <span class="pull-right text-yellow">@{{ times.leave }} ชม.</span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="text-blue">เวลาทำงานนอกสถานที่</span>
                        <span class="pull-right text-blue">@{{ times.onsite }} ชม.</span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="text-red">เวลาทำงานล่วงเวลา</span>
                        <span class="pull-right text-red">@{{ times.overtime }} ชม.</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- #Box Chart Time Usage -->
            
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="row">
            
            {{--<div class="col-md-12">--}}
              {{--<div class="box box-default">--}}
                {{--<div class="box-header with-border">--}}
                  {{--<h3 class="box-title">Direct Chat 3</h3>--}}
                {{--</div>--}}
                {{--<div class="box-body">--}}
                  {{--<span>123</span>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
            
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    
    $(function () {
    
    });
    
    const main = new Vue({
      el: '#main',
      data: {
        master: {},
        result: {},
        off_id: '{{ $profile->off_id }}',
        announces: [],
        times: {
          work: 0,
          leave: 0,
          onsite: 0,
          overtime: 0,
        },
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get(true);
        });
      },
      methods: {
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            officer_id: this.off_id
          };
          // _log(_data);
          axios.post('{{url('/api/dashboard')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            
            if (body.status === 'ok') {
              this.master = body.data;
              this.result = body.data.result;
              // _log(this.result);
              
              this.announces = this.result.announces;
              // _log(this.announces);
              
              this.times.work = this.result.times_work.time_work_hours;
              this.times.leave = this.result.times_work.time_noteleave_hours;
              this.times.onsite = this.result.times_work.time_onsite_hours;
              this.times.overtime = this.result.times_work.time_overtime_hours;
              this._setPieChart();
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _setPieChart: function () {
          let pieChartCanvas = $('#pieChart').get(0).getContext('2d');
          let pieChart = new Chart(pieChartCanvas);
          let PieData = [
            {
              value: main.times.work,
              color: '#00a65a',
              label: 'Work'
            },
            {
              value: main.times.leave,
              color: '#f39c12',
              label: 'Leave'
            },
            {
              value: main.times.onsite,
              color: '#00c0ef',
              label: 'OnSite'
            },
            {
              value: main.times.overtime,
              color: '#f56954',
              label: 'OverTime'
            },
          ];
          let pieOptions = {
            segmentShowStroke: true,
            segmentStrokeColor: '#fff',
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50,
            animationSteps: 90,
            animationEasing: 'easeOutBounce',
            animateRotate: true,
            animateScale: false,
            responsive: true,
            maintainAspectRatio: true
          };
          pieChart.Doughnut(PieData, pieOptions);
        }
      }
    });
  </script>
@endsection