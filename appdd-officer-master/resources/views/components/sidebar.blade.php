<aside class="main-sidebar">
  <section class="sidebar">
  
  <!-- search form -->
    <form class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="button" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->

    <?php
    function check_routes($routes) {
      $active = false;
      foreach ($routes as $route) {
        if (stripos(Request::path(), $route) === 0) {
          $active = true;
          break;
        }
      }
      return $active;
    }
    ?>
    
    <ul class="sidebar-menu" data-widget="tree">
      
      <?php if($profile->off_permission_level == 1) { ?>
      <li class="header"><i class="fa fa-user-circle"></i>&nbsp;&nbsp;&nbsp;ADMINISTRATOR</li>
      
      <?php $active = check_routes(['admin/dashboard']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('/admin/dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <?php $active = check_routes(['admin/officer']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('admin/officer')}}">
          <i class="fa fa-user-circle-o"></i> <span>Officer</span>
        </a>
      </li>

        <?php $active = check_routes(['admin/nl_report']); ?>
        <li class="<?php if ($active) echo 'active'; ?>">
          <a href="{{url('admin/nl_report')}}">
            <i class="fa fa-pie-chart"></i> <span>Note Leave Report</span>
          </a>
        </li>

      <?php $active = check_routes(['admin/noteleave']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('admin/noteleave')}}">
          <i class="fa fa-file-archive-o"></i> <span>Note Leave</span>
        </a>
      </li>

      <?php $active = check_routes(['admin/overtime']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('admin/overtime')}}">
          <i class="fa fa-clock-o"></i> <span>Over Time</span>
        </a>
      </li>

      <?php $active = check_routes(['admin/project']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('admin/project')}}">
          <i class="fa fa-folder-open"></i> <span>Project</span>
        </a>
      </li>

      <?php $active = check_routes(['admin/announce']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('admin/announce')}}">
          <i class="fa fa-bullhorn"></i> <span>Announce</span>
        </a>
      </li>

      <?php $active = check_routes(['admin/master']); ?>
      <li class="treeview <?php if ($active) echo 'menu-open'; ?>">
        <a href="#">
          <i class="fa fa-laptop"></i> <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <?php $active = check_routes(['admin/master/noteleavetype', 'admin/master/officertype']); ?>
        <ul class="treeview-menu" <?php if ($active) echo 'style="display: block;"'; ?>>
          <?php $active = check_routes(['admin/master/officertype']); ?>
          <li class="<?php if ($active) echo 'active'; ?>">
            <a href="{{url('admin/master/officertype')}}"><i class="fa fa-circle-o"></i> <span>Officer Type</span></a>
          </li>
          <?php $active = check_routes(['admin/master/noteleavetype']); ?>
          <li class="<?php if ($active) echo 'active'; ?>">
            <a href="{{url('admin/master/noteleavetype')}}"><i class="fa fa-circle-o"></i> <span>Note Leave Type</span></a>
          </li>
        </ul>
      </li>
      <?php } ?>
      
      
      <li class="header"><i class="fa fa-user-circle"></i>&nbsp;&nbsp;&nbsp;OFFICER MODULE</li>

      <?php $active = check_routes(['dashboard']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>แดชบอร์ด</span>
        </a>
      </li>
      
      <?php $active = check_routes(['noteleave']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('noteleave')}}">
          <i class="fa fa-file-archive-o"></i> <span>ใบลางาน</span>
        </a>
      </li>
      
      <?php $active = check_routes(['overtime']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('overtime')}}">
          <i class="fa fa-clock-o"></i> <span>ใบล่วงเวลา (OT)</span>
        </a>
      </li>

      <?php $active = check_routes(['changepassword']); ?>
      <li class="<?php if ($active) echo 'active'; ?>">
        <a href="{{url('changepassword')}}">
          <i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span>
        </a>
      </li>
      
    </ul>
  </section>
</aside>