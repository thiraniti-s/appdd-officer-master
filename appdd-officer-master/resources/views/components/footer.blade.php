<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.applicationdd.com">ApplicationDD</a></strong>.
</footer>