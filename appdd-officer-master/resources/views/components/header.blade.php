<header class="main-header">
  <a href="#" class="logo">
    <span class="logo-mini"><b>A</b>DO</span>
    <span class="logo-lg"><b>AppDD</b>Officer</span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?php if ($profile->off_image) { ?>
              <img src="{{ $profile->off_image }}" class="user-image" alt="User Image">
            <?php } else if ($profile->off_prefixname == 'นาย') { ?>
              <img src="{{asset('dist/img/avatar_man.png')}}" class="user-image" alt="User Image">
            <?php } else { ?>
              <img src="{{asset('dist/img/avatar_girl.png')}}" class="user-image" alt="User Image">
            <?php } ?>
            <span class="hidden-xs">{{ $profile->off_firstname }} {{ $profile->off_lastname }}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header" style="height: 198px;">
              <?php if ($profile->off_image) { ?>
                <img src="{{ $profile->off_image }}" class="img-circle" alt="User Image">
              <?php } else if ($profile->off_prefixname == 'นาย') { ?>
                <img src="{{asset('dist/img/avatar_man.png')}}" class="img-circle" alt="User Image">
              <?php } else { ?>
                <img src="{{asset('dist/img/avatar_girl.png')}}" class="img-circle" alt="User Image">
              <?php } ?>
              <p>
                {{ $profile->off_prefixname }}{{ $profile->off_firstname }} {{ $profile->off_lastname }}<br>
                - {{ $profile->off_position }} -
                <?php $start_work = (new DateTime($profile->off_start_working))->format('F Y'); ?>
                <small style="margin-top: 6px;">Start work since {{ $start_work }}</small>
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{url('/profile')}}" class="btn btn-default btn-flat rd4">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{url('/logout')}}" class="btn btn-default btn-flat rd4">Logout</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>