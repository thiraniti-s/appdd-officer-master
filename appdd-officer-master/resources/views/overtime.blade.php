@extends('layouts.master')

@section('content')
  <div class="content-wrapper" id="main" v-cloak>
    <section class="content-header">
      <h1>
        ใบล่วงเวลา
        <small>ใบล่วงเวลางานของคุณ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/profile')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="{{url('overtime')}}">ใบล่วงเวลา</a></li>
      </ol>
    </section>
  
    <section class="content">
      <div class="row">
  
        <!-- Box Request -->
        <div class="col-xs-12">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">รายการยื่นใบล่วงเวลา</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="180">วันที่</th>
                  <th class="text-td-center" width="150">เวลา</th>
                  <th class="text-td-center">บันทึกการทำงาน</th>
                  <th class="text-td-center" width="120">บุคคลอ้างอิง</th>
                  <th class="text-td-center" width="80">สถานะ</th>
                  <th class="text-td-center" width="60">ดูเพิ่ม</th>
                  <th class="text-td-center" width="60">ยกเลิก</th>
                </tr>
                <tr v-for="(data, index) in lists_request">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDate(data.ot_date) }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentTime(data.ot_time_start) }} - @{{ _momentTime(data.ot_time_end) }}
                  </td>
                  <td class="text-td-left">
                    @{{ data.ot_note }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.ot_reference }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-gray" v-if="data.ot_status==1">Waiting</span>
                    <span class="badge bg-yellow" v-else-if="data.ot_status==2">InReview</span>
                    <span class="badge bg-gray" v-else>Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-danger" @click="_cancel(data)" v-if="data.ot_status==1">
                      <i class="fa fa-trash"></i>
                    </button>
                    <span v-else>-</span>
                  </td>
                </tr>
                <tr v-if="lists_request.length == 0">
                  <td colspan="8" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box Request -->
  
        <!-- Box List -->
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">รายการใบล่วงเวลา - <?php echo (new DateTime())->format('M Y'); ?></h3>
            </div>
            {{--<pre v-if="lists.length > 0">@{{ lists[0] }}</pre>--}}
            <div class="box-body table-responsive no-padding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <th class="text-td-center" width="40">#</th>
                  <th class="text-td-center" width="180">วันที่</th>
                  <th class="text-td-center" width="150">เวลา</th>
                  <th class="text-td-center" width="100">คิดเป็น (ชม.)</th>
                  <th class="text-td-center">บันทึกการทำงาน</th>
                  <th class="text-td-center" width="120">บุคคลอ้างอิง</th>
                  <th class="text-td-center" width="80">สถานะ</th>
                  <th class="text-td-center" width="60">ดูเพิ่ม</th>
                </tr>
                <tr v-for="(data, index) in lists">
                  <td class="text-td-center">
                    @{{ index+1 }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentDate(data.ot_date) }}
                  </td>
                  <td class="text-td-center">
                    @{{ _momentTime(data.ot_time_start) }} - @{{ _momentTime(data.ot_time_end) }}
                  </td>
                  <td class="text-td-center">
                    <span v-if="data.ot_status==-1">---</span>
                    <span v-else>@{{ _momentDiffTimeWork(data.ot_time_start, data.ot_time_end) }}</span>
                  </td>
                  <td class="text-td-left" v-if="data.ot_status==-1">
                    @{{ data.ot_note }}<span class="text-red">@{{ !! data.ot_reject_comment ? ' ==> '+data.ot_reject_comment : '' }}</span>
                  </td>
                  <td class="text-td-left" v-else>
                    @{{ data.ot_note }}
                  </td>
                  <td class="text-td-center">
                    @{{ data.ot_reference }}
                  </td>
                  <td class="text-td-center">
                    <span class="badge bg-green" v-if="data.ot_status==3">Approve</span>
                    <span class="badge bg-red" v-else-if="data.ot_status==-1">Reject</span>
                    <span class="badge bg-gray" v-else>Unknown</span>
                  </td>
                  <td class="text-td-center">
                    <button type="button" class="btn btn-block btn-xs btn-default" @click="_detail(data)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                </tr>
                <tr v-if="lists.length == 0">
                  <td colspan="8" class="empty-data">
                    -- Data Not Found --
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #Box List -->
  
        <!-- Box Add -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ฟอร์มยื่นใบล่วงเวลา (OT)</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">บันทึกการทำงานล่วงเวลา (อธิบายงานที่ทำ ให้อ่านแล้วเข้าใจ)</label>
                  <input type="text" class="form-control" placeholder="" v-model="data.ot_note">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">วันที่</label>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" id="ot_date" value="<?php echo date('Y-m-d'); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">บุคคลอ้างอิง (ระบุชื่อผู้ที่อนุมัติให้ทำงานล่วงเวลา)</label>
                  <input type="text" class="form-control" placeholder="" v-model="data.ot_reference">
                </div>
                <div class="form-group">
                  <label>เวลา</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="ot_time_start" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="bootstrap-timepicker">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="ot_time_end" readonly>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="button" class="btn btn-primary" @click="_submit()">ยื่นใบล่วงเวลา</button>
              <button type="button" class="btn btn-default" @click="_clear()">เคลียร์</button>
            </div>
          </div>
        </div>
        <!-- #Box Add -->
        
      </div>
    </section>
  </div>
@endsection

@section('script')
  <script>
    $(function () {
      $('.select2').select2();
      $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate: moment().date(1).format('YYYY-MM-DD'),
        endDate: moment().endOf('month').format('YYYY-MM-DD')
      });
      $('#ot_time_start').timepicker({
        defaultTime: '09:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false,
      });
      $('#ot_time_end').timepicker({
        defaultTime: '18:00',
        showInputs: false,
        minuteStep: 30,
        showMeridian: false
      });
    });
    
    new Vue({
      el: '#main',
      data: {
        master: {},
        lists: [],
        lists_request: [],
        off_id: '{{ $profile->off_id }}',
        off_firstname: '{{ $profile->off_firstname }}',
        params: {
          search: null,
          filter: {},
          page: 1,
          limit: 100
        },
        data: {
          ot_note: '',
          ot_reference: '',
          ot_date: moment().format('YYYY-MM-DD'),
          ot_time_start: '09:00',
          ot_time_end: '18:00'
        }
      },
      mounted: function () {
        this.$nextTick(function () {
          this._get(true);
        })
      },
      methods: {
        _get: function (loading) {
          if (loading) {
            _nextOpenLoading();
          }
          let _data = {
            off_id: this.off_id,
            search: this.params.search,
            filter: this.params.filter,
            page: this.params.page,
            limit: this.params.limit
          };
          // _log(_data);
          axios.post('{{url('/api/overtime')}}', _data).then(response => {
            let body = response.data;
            // _log(body);
            _nextCloseLoading();
            if (body.status === 'ok') {
              this.master = body.data;
              let result = body.data.result;

              this.lists = result.filter((data) => {
                if (data.ot_status === 3 || data.ot_status === -1) {
                  return data
                }
              });
              this.lists_request = result.filter((data) => {
                if (data.ot_status === 1 || data.ot_status === 2) {
                  return data
                }
              });
            }
            else {
              _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
            }
          }).catch(error => {
            _nextCloseLoading();
            _nextAlert(error.message, 'err');
          });
        },
        _clear: function () {
          $('.select2').val('').trigger('change');
          this.data.ot_note = '';
          this.data.ot_reference = '';
          this.data.ot_date = moment().format('YYYY-MM-DD');
          this.data.ot_time_start = '09:00';
          this.data.ot_time_end = '18:00';
          $('#ot_date').val(moment().format('YYYY-MM-DD'));
          $('#ot_time_start').val('09:00');
          $('#ot_time_end').val('18:00');
        },
        _validate: function () {
          this.data.ot_date = $('#ot_date').val();
          this.data.ot_time_start = $('#ot_time_start').val();
          this.data.ot_time_end = $('#ot_time_end').val();
          
          if (!_nextIsNotValid(this.data.ot_note, '', 'กรุณากรอกบันทึกการทำงานล่วงเวลา')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_reference, '', 'กรุณากรอกบุคคลอ้างอิง')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_date, '', 'กรุณาเลือกวันที่')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_time_start, '', 'กรุณาเลือกเวลาเริ่ม')) {
            return false;
          }
          if (!_nextIsNotValid(this.data.ot_time_end, '', 'กรุณาเลือกเวลาสิ้นสุด')) {
            return false;
          }
          if (!moment(this.data.ot_time_start, 'HH:ss').isBefore(moment(this.data.ot_time_end, 'HH:ss'))) {
            swal({
              text: 'เลือกเวลาไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง',
              icon: 'warning'
            });
            return false;
          }
          
          return true;
        },
        _submit: function () {
          if (this._validate()) {
            swal({
              title: 'ยืนยันการส่งใบล่วงเวลา',
              text: 'ต้องการส่งใบล่วงเวลาพร้อมกับข้อมูลนี้ใช่หรือไม่ ?',
              icon: 'info',
              buttons: true,
              closeOnClickOutside: false
            }).then(confirm => {
              if (confirm) {
                _nextOpenLoading();
                let data = {
                  off_id: this.off_id,
                  ot_note: this.data.ot_note,
                  ot_reference: this.data.ot_reference,
                  ot_date: this.data.ot_date,
                  ot_time_start: this.data.ot_time_start,
                  ot_time_end: this.data.ot_time_end
                };
                // _log(data);
                axios.post('{{url('/api/overtime/insert')}}', data).then(response => {
                  let body = response.data;
                  // _log(body);
                  _nextCloseLoading();
                  if (body.status === 'ok') {
                    swal({
                      title: 'ทำรายการสำเร็จ',
                      text: 'ส่งข้อมูลถึงผู้ดูแลระบบแล้ว โปรดรอการอนุมัติ',
                      icon: 'success',
                      closeOnClickOutside: false
                    }).then(ok => {
                      this._clear();
                      this._get(true);
                    });
                  }
                  else {
                    _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                  }
                }).catch(error => {
                  _nextCloseLoading();
                  _nextAlert(error.message, 'err');
                });
              }
            });
          }
        },
        _cancel: function (data) {
          swal({
            title: 'ยืนยันการเปลี่ยนสถานะ',
            text: 'เปลี่ยนสถานะของใบล่วงเวลาเป็น "ยกเลิก" ?',
            icon: 'info',
            buttons: [true, 'Confirm'],
            closeOnClickOutside: false
          }).then(confirm => {
            if (confirm) {
              let _data = {
                off_id: this.off_id,
                ot_id: data.ot_id
              };
              axios.post('{{url('/api/overtime/status/cancel')}}', _data).then(response => {
                let body = response.data;
                // _log(body);
                _nextCloseLoading();
                if (body.status === 'ok') {
                  swal({
                    text: 'ทำรายการสำเร็จ',
                    icon: 'success',
                    closeOnClickOutside: false
                  }).then(ok => {
                    this._clear();
                    this._get(true);
                  });
                }
                else {
                  _nextAlert('ทำรายการไม่สำเร็จ: ' + body.message, 'war');
                }
              }).catch(error => {
                _nextCloseLoading();
                _nextAlert(error.message, 'err');
              });
            }
          });
        },
        _detail: function (data) {
          _nextAlert('This featured is coming soon', 'inf', 'AppDD Officer');
        }
      }
    });
  </script>
@endsection