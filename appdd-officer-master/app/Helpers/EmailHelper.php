<?php

namespace App\Helpers;

/*
 * Created by Kwanpon Wattanaklang
 * Application DD Limited Partnership
 * http://www.applicationdd.com
 * DateTime 22/5/18
 */

use Illuminate\Support\Facades\Mail;

class EmailHelper {

    private static $email_admins = array(
        'kwanpon@applicationdd.com',
        'boyd@applicationdd.com',
        'mix@applicationdd.com'
    );
	
	public static function sendOfficer($to, $subject, $title, $body_message) {
	    $data = [
            'title' => $title,
            'body_message' => $body_message
        ];
        Mail::send('emails.send', $data, function ($message) use ($to, $subject) {
            $message->to($to);
            $message->subject($subject);
        });
	}

    public static function sendAdmin($subject, $title, $body_message) {
        $data = [
            'title' => $title,
            'body_message' => $body_message
        ];
        Mail::send('emails.send', $data, function ($message) use ($subject) {
            $message->to(self::$email_admins);
            $message->subject($subject);
        });
    }

    public static function sendGenerateAccount($to, $name, $username, $password) {
        $data = [
            'name' => $name,
            'username' => $username,
            'password' => $password
        ];
        Mail::send('emails.gen_account', $data, function ($message) use ($to) {
            $message->to($to);
            $message->subject('Application DD Account');
        });
    }

    public static function createNoteLeave($subject, $name, $type, $note, $date, $time) {
        $data = [
            'name' => $name,
            'type' => $type,
            'note' => $note,
            'date' => $date,
            'time' => $time
        ];
        Mail::send('emails.create_noteleave', $data, function ($message) use ($subject) {
            $message->to(self::$email_admins);
            $message->subject($subject);
        });
    }

    public static function updateNoteLeave($to, $subject, $name, $status) {
        $data = [
            'name' => $name,
            'status' => $status
        ];
        Mail::send('emails.update_noteleave', $data, function ($message) use ($to, $subject) {
            $message->to($to);
            $message->subject($subject);
        });
    }

    public static function createOverTime($subject, $name, $note, $reference, $date, $time) {
        $data = [
            'name' => $name,
            'note' => $note,
            'reference' => $reference,
            'date' => $date,
            'time' => $time
        ];
        Mail::send('emails.create_overtime', $data, function ($message) use ($subject) {
            $message->to(self::$email_admins);
            $message->subject($subject);
        });
    }

    public static function updateOverTime($to, $subject, $name, $status) {
        $data = [
            'name' => $name,
            'status' => $status
        ];
        Mail::send('emails.update_overtime', $data, function ($message) use ($to, $subject) {
            $message->to($to);
            $message->subject($subject);
        });
    }
	
}
