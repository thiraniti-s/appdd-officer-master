<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class FileHelper {
	
	public static function putFile($folder, $file) {
		return ($file != null) ? Storage::putFile($folder, $file) : null;
	}
	
	public static function getFullPath($file_name) {
		return ($file_name != null) ? asset('storage/'.$file_name) : null;
	}
	
}
