<?php

namespace App\Helpers;

/*
 * Created by Kwanpon Wattanaklang
 * Application DD Limited Partnership
 * http://www.applicationdd.com
 * DateTime 19/3/19
 */

use Carbon\Carbon;
use GuzzleHttp;

class SlackHelper
{
    protected static $slack_hook_url = "https://hooks.slack.com/services";

    private static function getWebHookUrl($key)
    {
        return self::$slack_hook_url . '/' . config('app.slack_workspace_key') . '/' . $key;
    }

    public static function sendChannel($channel, $message)
    {
        $key = config('app.slack_channel_' . strtolower($channel));
        if ($key != null) {
            $client = new GuzzleHttp\Client();
            $res = $client->post(self::getWebHookUrl($key), [
                'json' => ['text' => $message]
            ]);
            return $res;
        } else {
            return null;
        }
    }

    public static function sendNLChannel($channel, $message, $title, $title_link, $nl_type, $nl_note, $nl_date, $nl_time)
    {
        $key = config('app.slack_channel_' . strtolower($channel));
        if ($key != null) {
            $client = new GuzzleHttp\Client();
            $res = $client->post(self::getWebHookUrl($key), [
                'json' => [
                    'text' => $message,
                    'attachments' => [
                        [
                            'fallback' => $title . ' - ' . $title_link,
                            'title' => $title,
                            'title_link' => $title_link ?? null,
                            'color' => '#7CD197',
                            'text' => null,
                            'mrkdwn_in' => ['text'],
                            'fields' => [
                                [
                                    'title' => 'Type',
                                    'value' => $nl_type,
                                    'short' => true
                                ],
                                [
                                    'title' => 'Note',
                                    'value' => $nl_note,
                                    'short' => true
                                ],
                                [
                                    'title' => 'Date',
                                    'value' => $nl_date,
                                    'short' => true
                                ],
                                [
                                    'title' => 'Time',
                                    'value' => $nl_time,
                                    'short' => true
                                ]
                            ],
                            'footer' => 'Officer App DD x Slack API - ' . Carbon::now()->toDateTimeString(),
                            'footer_icon' => 'https://platform.slack-edge.com/img/default_application_icon.png',
                        ]
                    ]
                ]
            ]);
            return $res;
        } else {
            return null;
        }
    }

    public static function sendNLDM($slack_token, $message)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(self::getWebHookUrl($slack_token), [
            'json' => [
                'text' => $message
            ]
        ]);
        return $res;
    }

    public static function sendDM($slack_token, $message, $title, $title_link, $nl_type, $nl_note, $nl_date, $nl_time)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(self::getWebHookUrl($slack_token), [
            'json' => [
                'text' => $message,
                'attachments' => [
                    [
                        'fallback' => $title . ' - ' . $title_link,
                        'title' => $title,
                        'title_link' => $title_link ?? null,
                        'color' => '#7CD197',
                        'text' => null,
                        'mrkdwn_in' => ['text'],
                        'fields' => [
                            [
                                'title' => 'Type',
                                'value' => $nl_type,
                                'short' => true
                            ],
                            [
                                'title' => 'Note',
                                'value' => $nl_note,
                                'short' => true
                            ],
                            [
                                'title' => 'Date',
                                'value' => $nl_date,
                                'short' => true
                            ],
                            [
                                'title' => 'Time',
                                'value' => $nl_time,
                                'short' => true
                            ]
                        ],
                        'footer' => 'Officer App DD x Slack API - ' . Carbon::now()->toDateTimeString(),
                        'footer_icon' => 'https://platform.slack-edge.com/img/default_application_icon.png',
                    ]
                ]
            ]
        ]);
        return $res;
    }

}
