<?php

namespace App\Helpers;

/*
 * Created by Kwanpon Wattanaklang
 * Application DD Limited Partnership
 * http://www.applicationdd.com
 * DateTime 27/4/18
 */

class FuncHelper {

    public static function response() {
        return array(
            'status' => 'ok',
            'message' => '',
            'data' => null
        );
    }

    public static function responseError($message) {
        return array(
            'status' => 'error',
            'message' => $message,
            'data' => null
        );
    }

    public static function responseData($data) {
        return self::responseResult(null, null, count($data), 1, count($data), $data);
    }

    public static function responseResult($search, $filters, $count_all_data, $page, $limit, $data) {
        return array(
            'search' => $search,
            'filters' => $filters,
            'total' => $count_all_data,
            'page' => intval($page),
            'limit' => intval($limit),
            'total_pages' => ceil($count_all_data/$limit),
            'total_result' => count($data),
            'result' => $data
        );
    }

    public static function checkRequiredParams($params_required) {
        $res = array(
            'success' => true,
            'message' => ''
        );

        $is_missing_param = false;
        $param_is_missing = "";

        for ($i = 0; $i < count($params_required); $i++) {
            $key = array_keys($params_required)[$i];
            $val = $params_required[$key] ?? null;

            if ($val === null) {
                $is_missing_param = true;
                $param_is_missing = $key;
                break;
            }
        }

        if ($is_missing_param) {
            $res['success'] = false;
            $res['message'] = 'Missing param \'' . $param_is_missing . '\'';
        }

        return (object) $res;
    }

    public static function readableDate($date) {
        $month_thai = [
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.'
        ];

        $timeunix = strtotime($date.' 00:00:00');
        $day = date('j', $timeunix);
        $month = $month_thai[date('n', $timeunix)-1];
        $year = date('Y', $timeunix);

        return $day.' '.$month.' '.$year;
    }

}
