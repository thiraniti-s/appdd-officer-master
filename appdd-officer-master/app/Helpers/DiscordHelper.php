<?php

namespace App\Helpers;

/*
 * Application DD Limited Partnership
 * http://www.applicationdd.com
 * DateTime 30/11/21
 */

use Carbon\Carbon;
use GuzzleHttp;

class DiscordHelper
{
    protected static $webhook_url = "https://discord.com/api/webhooks";

    private static function getWebHookUrl($key)
    {
        return self::$webhook_url . '/' . $key;
    }

    public static function sendNLChannel($channel, $content, $title, $description)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->post(self::getWebHookUrl($channel), [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'content' => $content,
                'embeds' => [
                    [
                        'title' => $title,
                        'description' => $description,
                        'color' => '7506394',
                    ]
                ],
            ]
            
        ]);
        return self::getWebHookUrl($channel);
    }
}
