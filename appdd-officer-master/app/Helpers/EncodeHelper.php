<?php

namespace App\Helpers;

/*
 * Created by Kwanpon Wattanaklang
 * Application DD Limited Partnership
 * http://www.applicationdd.com
 * DateTime 27/4/18
 */

class EncodeHelper {
	
	public static function getSHA256($password) {
		$salt = 'appdd.officer';
		$password_hash = hash_hmac('sha256', $password, $salt);
		return $password_hash;
	}
	
}
