<?php

namespace App\Http\Controllers\User;

use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\Announce;
use App\Models\NoteLeave;
use App\Models\OverTime;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getDashboard(Request $request)
    {
        $officer_id = $request->input('officer_id');

        $response = FuncHelper::response();

        $params_required = array(
            'officer_id' => $officer_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $data = array();

            $announces = Announce::where('ann_status', 1)->where('ann_date', '<=', Carbon::today())->orderBy('ann_id', 'desc')->get()->take(5);
            foreach ($announces as $announce) {
                $announce->officer;
            }
            $data['announces'] = $announces;

            $d = 1;
            $count_day_works = 0;
            $date_in_month = [];
            while (true) {
                $date = Carbon::parse(Carbon::now()->format('Y-m-') . $d);
                $date_in_month[] = $date->format('Y-m-d');
                if (!$date->isSaturday() && !$date->isSunday()) {
                    $count_day_works++;
                }
                if ($date->isToday()) {
                    break;
                }
                $d++;
            }
            $time_work_hours = $count_day_works * 8;
            $time_noteleave_hours = 0;
            $time_onsite_hours = 0;
            $time_overtime_hours = 0;

            $noteleaves = NoteLeave::where('off_id', $officer_id)->whereIn('nl_leave_start', $date_in_month)->whereIn('nl_status', [2, 3])->get();
            foreach ($noteleaves as $noteleave) {
                if ($noteleave->nl_type_id < 4) {
                    if ($noteleave->nl_time_type == 1) { // full-day
                        if ($noteleave->nl_leave_start != $noteleave->nl_leave_end) { // more days
                            $diff_days = Carbon::parse($noteleave->nl_leave_start)->diffInDays(Carbon::parse($noteleave->nl_leave_end));
                            $time_noteleave_hours += ($diff_days * 8);
                        } else { // 1 day
                            $time_noteleave_hours += 8;
                        }
                    } else { // sometimes
                        $time1 = Carbon::parse($noteleave->nl_time_start);
                        $time2 = Carbon::parse($noteleave->nl_time_end);
                        $diff_hours = $time1->diffInHours($time2);
                        $cross_lunch = (Carbon::parse('12:00:01')->between($time1, $time2) && ($time2 > Carbon::parse('12:59:59')));
                        if ($cross_lunch) {
                            $diff_hours -= 1;
                        }
                        $time_noteleave_hours += $diff_hours;
                    }
                } else { // on-site
                    if ($noteleave->nl_time_type == 1) { // full-day
                        if ($noteleave->nl_leave_start != $noteleave->nl_leave_end) { // more days
                            $diff_days = Carbon::parse($noteleave->nl_leave_start)->diffInDays(Carbon::parse($noteleave->nl_leave_end));
                            $time_onsite_hours += ($diff_days * 8);
                        } else { // 1 day
                            $time_onsite_hours += 8;
                        }
                    } else { // sometimes
                        $time1 = Carbon::parse($noteleave->nl_time_start);
                        $time2 = Carbon::parse($noteleave->nl_time_end);
                        $diff_hours = $time1->diffInHours($time2);
                        $cross_lunch = (Carbon::parse('12:00:01')->between($time1, $time2) && ($time2 > Carbon::parse('12:59:59')));
                        if ($cross_lunch) {
                            $diff_hours -= 1;
                        }
                        $time_onsite_hours += $diff_hours;
                    }
                }
            }

            $overtimes = OverTime::where('off_id', $officer_id)->whereIn('ot_date', $date_in_month)->whereIn('ot_status', [2, 3])->get();
            foreach ($overtimes as $overtime) {
                $diff_hours = Carbon::parse($overtime->ot_time_start)->diffInHours(Carbon::parse($overtime->ot_time_end));
                $time_overtime_hours += $diff_hours;
            }

            $data['times_work'] = [
                'time_noteleave_hours' => $time_noteleave_hours,
                'time_onsite_hours' => $time_onsite_hours,
                'time_overtime_hours' => $time_overtime_hours,
                'time_work_hours' => $time_work_hours - $time_noteleave_hours - $time_onsite_hours
            ];

            $response['data'] = FuncHelper::responseData($data);
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}