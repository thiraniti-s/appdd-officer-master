<?php

namespace App\Http\Controllers\User;

use App\Helpers\EmailHelper;
use App\Helpers\FuncHelper;
use App\Helpers\SlackHelper;
use App\Helpers\DiscordHelper;
use App\Http\Controllers\Controller;
use App\Models\ErrorLog;
use App\Models\NoteLeave;
use App\Models\NoteLeaveType;
use App\Models\Officer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NoteLeaveController extends Controller
{
    public function getNoteLeave(Request $request)
    {
        $off_id = $request->input('off_id');
        $nl_month = $request->input('nl_month') ?? Carbon::now('GMT+7')->format('m');
        $nl_year = $request->input('nl_year') ?? Carbon::now('GMT+7')->format('Y');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeave::where('off_id', $off_id)
                    ->whereIn('nl_status', [1, 2, 3, -1])
                    ->whereMonth('nl_leave_start', strval($nl_month))
                    ->whereYear('nl_leave_start', strval($nl_year))
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'nl_note'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('nl_leave_start', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($nl_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
                $data->note_leave_type_data = NoteLeaveType::where('nl_type_id', $data->nl_type_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertNoteLeave(Request $request)
    {
        $off_id = $request->input('off_id');
        $nl_type_id = $request->input('nl_type_id');
        $nl_time_type = $request->input('nl_time_type');
        $nl_note = $request->input('nl_note');
        $nl_leave_start = $request->input('nl_leave_start');
        $nl_leave_end = $request->input('nl_leave_end');
        $nl_time_start = $request->input('nl_time_start');
        $nl_time_end = $request->input('nl_time_end');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
            'nl_type_id' => $nl_type_id,
            'nl_time_type' => $nl_time_type,
            'nl_note' => $nl_note,
            'nl_leave_start' => $nl_leave_start,
            'nl_leave_end' => $nl_leave_end
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = NoteLeave::create([
                'off_id' => $off_id,
                'nl_type_id' => $nl_type_id,
                'nl_time_type' => $nl_time_type,
                'nl_note' => $nl_note,
                'nl_leave_start' => $nl_leave_start,
                'nl_leave_end' => $nl_leave_end,
                'nl_time_start' => $nl_time_start,
                'nl_time_end' => $nl_time_end,
                'nl_status' => 1
            ]);

            $off_data = Officer::where('off_id', $off_id)->first();
            $nl_type_data = NoteLeaveType::where('nl_type_id', $nl_type_id)->first();
            $id_format = str_pad($model->id, 5, '0', STR_PAD_LEFT);

            // send email if production
            if (config('app.env') == 'production') {
                $subject = 'ยื่นใบลางาน จาก ' . $off_data->off_firstname . ' (NTID:' . $id_format . ')';
                $name = $off_data->off_firstname . ' ' . $off_data->off_lastname . ' (' . $off_data->off_nickname . ')';
                $type = $nl_type_data->nl_type_name;
                if ($nl_leave_start == $nl_leave_end) {
                    $date = FuncHelper::readableDate($nl_leave_start);
                } else {
                    $date = FuncHelper::readableDate($nl_leave_start) . ' ถึง ' . FuncHelper::readableDate($nl_leave_end);
                }
                if ($nl_time_type == 2) {
                    $time = $nl_time_start . ' - ' . $nl_time_end;
                } else {
                    $time = '-';
                }
                try {
                    EmailHelper::createNoteLeave($subject, $name, $type, $nl_note, $date, $time);
                } catch (\Exception $e) {
                    ErrorLog::create([
                        'err_source' => 'NoteLeaveController@insertNoteLeave - EmailHelper',
                        'err_exception' => $e->getMessage()
                    ]);
                }
            }

            // send slack notification
            try {
                $message = 'ยื่นใบลา - ' . $off_data->off_firstname . ' ' . $off_data->off_lastname . ' (' . $off_data->off_nickname . ')';
                $title = 'NoteLeave #' . $id_format;
                $type = $nl_type_data->nl_type_name;
                if ($nl_leave_start == $nl_leave_end) {
                    $date = FuncHelper::readableDate($nl_leave_start);
                } else {
                    $date = FuncHelper::readableDate($nl_leave_start) . ' ถึง ' . FuncHelper::readableDate($nl_leave_end);
                }
                if ($nl_time_type == 2) {
                    $time = $nl_time_start . ' - ' . $nl_time_end;
                } else {
                    $time = '-';
                }

                $discordNLChannelKey = '915106483107151923/DvyM0ukQ5qTzF7HRV3uaEJ-zikNhBqinhLEGvDs2U2d8QebRPbklajPVLhYQ-gfkxZnM';

                $description = $type . ' วันที่ ' . $date;

                if ($time !== '-') {
                    $description = $description . ' เวลา ' . $time;
                }

                DiscordHelper::sendNLChannel($discordNLChannelKey, $title, $message, $description);
                
                SlackHelper::sendNLChannel(
                    'team_lead', $message, $title,
                    'http://officer.applicationdd.com/admin/noteleave', $type, $nl_note, $date, $time
                );
//                SlackHelper::sendNLDM(
//                    $off_data->off_slack_webhook_token,
//                    'คุณได้ทำการยื่นใบลา <http://officer.applicationdd.com/noteleave|*NoteLeave #' . $id_format . '*> กรุณารอการอนุมัติ'
//                );
            } catch (\Exception $e) {
                ErrorLog::create([
                    'err_source' => 'NoteLeaveController@insertNoteLeave - SalckHelper',
                    'err_exception' => $e->getMessage()
                ]);
                FuncHelper::responseError($e->getMessage());
            }

            $nl_data = NoteLeave::where('nl_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateNoteLeaveCancel(Request $request)
    {
        $off_id = $request->input('off_id');
        $nl_id = $request->input('nl_id');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
            'nl_id' => $nl_id
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            NoteLeave::where('nl_id', $nl_id)
                ->update([
                    'nl_status' => 0
                ]);

            $off_data = Officer::where('off_id', $off_id)->first();
            $id_format = str_pad($nl_id, 5, '0', STR_PAD_LEFT);

//            SlackHelper::sendNLDM(
//                $off_data->off_slack_webhook_token,
//                'คุณได้ทำการยกเลิกการยื่นใบลา <http://officer.applicationdd.com/noteleave|*NoteLeave #' . $id_format . '*> เรียบร้อยแล้ว'
//            );
            SlackHelper::sendChannel(
                'team_lead',
                $off_data->off_firstname . ' ยกเลิกการยื่นใบลา <http://officer.applicationdd.com/noteleave|*NoteLeave #' . $id_format . '*> แล้ว'
            );

            $nl_data = NoteLeave::where('nl_id', $nl_id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}