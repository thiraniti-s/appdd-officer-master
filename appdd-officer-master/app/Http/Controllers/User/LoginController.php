<?php

namespace App\Http\Controllers\User;

use App\Helpers\EncodeHelper;
use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\Officer;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $off_username = $request->input('off_username');
        $off_password = $request->input('off_password');

        $response = FuncHelper::response();

        $params_required = array(
            'off_username' => $off_username,
            'off_password' => $off_password
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $off_data = Officer::query()
                ->select(
                    'off_id',
                    'off_code',
                    'off_prefixname',
                    'off_firstname',
                    'off_lastname',
                    'off_nickname',
                    'off_image',
                    'off_tel',
                    'off_email',
                    'off_position',
                    'off_username',
                    'off_permission_level',
                    'off_start_working',
                    'off_status'
                )
                ->where('off_username', $off_username)
                ->where('off_password', EncodeHelper::getSHA256($off_password))
                ->first();

            if ($off_data) {
                if ($off_data->off_status == 0) {
                    return FuncHelper::responseError('สถานะของพนักงานไม่สามารถใช้งานระบบได้');
                }

                $response['data'] = $off_data;
                // save session
                session(['login' => $off_data]);
                session(['username' => $off_data->off_username]);
                return $response;
            } else {
                return FuncHelper::responseError('Username or password incorrected !!');
            }
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}