<?php

namespace App\Http\Controllers\User;

use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\NoteLeaveType;
use Illuminate\Http\Request;

class NoteLeaveTypeController extends Controller
{
    public function getNoteLeaveType(Request $request)
    {
        $off_id = $request->input('off_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeaveType::where('nl_type_status', 1)
                    ->where('nl_type_level', 1)
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'nl_type_name'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_type_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('nl_type_id', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_type_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}