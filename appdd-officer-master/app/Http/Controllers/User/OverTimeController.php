<?php

namespace App\Http\Controllers\User;

use App\Helpers\EmailHelper;
use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\Officer;
use App\Models\OverTime;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OverTimeController extends Controller
{
    public function getOverTime(Request $request)
    {
        $off_id = $request->input('off_id');
        $ot_month = $request->input('ot_month') ?? Carbon::now('GMT+7')->format('m');
        $ot_year = $request->input('ot_year') ?? Carbon::now('GMT+7')->format('Y');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = OverTime::where('off_id', $off_id)
                    ->whereIn('ot_status', [1, 2, 3, -1])
                    ->whereMonth('ot_date', strval($ot_month))
                    ->whereYear('ot_date', strval($ot_year))
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'ot_note',
                                'ot_reference'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $ot_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('ot_date', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($ot_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $ot_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertOverTime(Request $request)
    {
        $off_id = $request->input('off_id');
        $ot_note = $request->input('ot_note');
        $ot_reference = $request->input('ot_reference');
        $ot_date = $request->input('ot_date');
        $ot_time_start = $request->input('ot_time_start');
        $ot_time_end = $request->input('ot_time_end');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
            'ot_note' => $ot_note,
            'ot_reference' => $ot_reference,
            'ot_date' => $ot_date,
            'ot_time_start' => $ot_time_start,
            'ot_time_end' => $ot_time_end
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = OverTime::create([
                'off_id' => $off_id,
                'ot_note' => $ot_note,
                'ot_reference' => $ot_reference,
                'ot_date' => $ot_date,
                'ot_time_start' => $ot_time_start,
                'ot_time_end' => $ot_time_end,
                'ot_status' => 1
            ]);

            // send email if production
            if (config('app.env') == 'production') {
                $off_data = Officer::where('off_id', $off_id)->first();

                $subject = 'ยื่นใบล่วงเวลา จาก ' . $off_data->off_firstname . ' (OTID:' . str_pad($model->id, 5, '0', STR_PAD_LEFT) . ')';
                $name = $off_data->off_firstname . ' ' . $off_data->off_lastname . ' (' . $off_data->off_nickname . ')';
                $date = FuncHelper::readableDate($ot_date);
                $time = $ot_time_start . ' - ' . $ot_time_end;
                try {
                    EmailHelper::createOverTime($subject, $name, $ot_note, $ot_reference, $date, $time);
                } catch (\Exception $e) {
                }
            }

            $ot_data = OverTime::where('ot_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($ot_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateOverTimeCancel(Request $request)
    {
        $off_id = $request->input('off_id');
        $ot_id = $request->input('ot_id');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
            'ot_id' => $ot_id
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            OverTime::where('ot_id', $ot_id)
                ->update([
                    'ot_status' => 0
                ]);

            $ot_data = OverTime::where('ot_id', $ot_id)->first();
            $response['data'] = FuncHelper::responseData(array($ot_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}