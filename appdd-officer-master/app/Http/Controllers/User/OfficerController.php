<?php

namespace App\Http\Controllers\User;

use App\Helpers\EmailHelper;
use App\Helpers\EncodeHelper;
use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\Officer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class OfficerController extends Controller
{
    public function generateAccount(Request $request)
    {
        $off_personal_id = $request->input('off_personal_id');
        $off_username = $request->input('off_username');

        $params_required = array(
            'off_personal_id' => $off_personal_id,
            'off_username' => $off_username
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $off_data = Officer::where('off_personal_id', $off_personal_id)
                ->where('off_status', 1)
                ->first();

            if ($off_data) {
                if ($off_data->off_username) {
                    return FuncHelper::responseError('This officer is already set username');
                }

                if (Officer::where('off_username', $off_username)->count() > 0) {
                    return FuncHelper::responseError('Username is invalid');
                }

                $password = $this->generatePassword(6);
                try {
                    Officer::where('off_personal_id', $off_personal_id)
                        ->update([
                            'off_username' => $off_username,
                            'off_password' => EncodeHelper::getSHA256(md5($password))
                        ]);

                    // send email
                    EmailHelper::sendGenerateAccount($off_data->off_email, $off_data->off_firstname . ' ' . $off_data->off_lastname, $off_username, $password);

                    $off_data = Officer::where('off_personal_id', $off_personal_id)
                        ->where('off_status', 1)
                        ->first();

                    return FuncHelper::responseData(array($off_data));

                } catch (QueryException $e) {
                    return FuncHelper::responseError($e->getMessage());
                } catch (\Exception $e) {
                    return FuncHelper::responseError($e->getMessage());
                }
            } else {
                return FuncHelper::responseError('Invalid off_personal_id');
            }
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function changePassword(Request $request)
    {
        $off_id = $request->input('off_id');
        $old_password = $request->input('old_password');
        $new_password = $request->input('new_password');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
            'old_password' => $old_password,
            'new_password' => $new_password
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            // check old password
            if (Officer::where('off_id', $off_id)
                    ->where('off_password', EncodeHelper::getSHA256(md5($old_password)))
                    ->count() > 0) {

                // update new password
                try {
                    Officer::where('off_id', $off_id)
                        ->update([
                            'off_password' => EncodeHelper::getSHA256(md5($new_password))
                        ]);

                    $data = Officer::where('off_id', $off_id)->first();
                    $response['data'] = array($data);
                    return $response;
                } catch (QueryException $e) {
                    return FuncHelper::responseError($e->getMessage());
                }
            } else {
                return FuncHelper::responseError('รหัสผ่านเดิมไม่ถูกต้อง กรุณาลองใหม่');
            }
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getOfficer(Request $request)
    {
        $off_id = $request->input('off_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = Officer::where('off_id', $off_id)
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'off_firstname',
                                'off_lastname',
                                'off_nickname'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $off_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($off_data as $data) {
                $data->off_type;
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $off_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    // private functions

    private function generatePassword($length = 8)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz123456789';
        $charactersLength = strlen($characters);
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= $characters[rand(0, $charactersLength - 1)];
        }
        return $password;
    }

}