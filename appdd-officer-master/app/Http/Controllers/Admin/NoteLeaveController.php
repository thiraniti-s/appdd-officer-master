<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\EmailHelper;
use App\Helpers\SlackHelper;
use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\NoteLeave;
use App\Models\NoteLeaveType;
use App\Models\Officer;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoteLeaveController extends Controller
{
    public function getNoteLeave(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $nl_month = $request->input('nl_month') ?? null;
        $nl_year = $request->input('nl_year') ?? null;
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;
        $officer = $request->input('officer_id') ?? "all";

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeave::whereIn('nl_status', [1, 2, 3, -1]);
                if($nl_month != null){
                    $query->whereMonth('nl_leave_start', strval($nl_month));
                }
                if($nl_year != null){
                    $query->whereYear('nl_leave_start', strval($nl_year));
                }
                if($officer != "all"){
                    $query->where('off_id',$officer);
                }
                    $query->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'nl_note'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->orderBy('nl_leave_start', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($nl_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
                $data->note_leave_type_data = NoteLeaveType::where('nl_type_id', $data->nl_type_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getNoteLeaveRequest(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeave::whereIn('nl_status', [1,2]);
                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->orderBy('nl_leave_start', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($nl_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
                $data->note_leave_type_data = NoteLeaveType::where('nl_type_id', $data->nl_type_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getNoteLeaveMonth()
    {
        $response = FuncHelper::response();

        $months = NoteLeave::select(DB::raw('DATE_FORMAT(nl_leave_start, "%Y-%m") as idkey'))
            ->groupby('idkey')
            ->orderByDesc('idkey')
            ->limit(12)
            ->get();

        $month_str = [
            'มกราคม',
            'กุมภาพันธ์',
            'มีนาคม',
            'เมษายน',
            'พฤษภาคม',
            'มิถุนายน',
            'กรกฎาคม',
            'สิงหาคม',
            'กันยายน',
            'ตุลาคม',
            'พฤศจิกายน',
            'ธันวาคม'
        ];
//        $datas = [];
//        if (count($months) > 0) {
//            $current_month_key = Carbon::now()->format('Y-m');
//            if ($months[0]->idkey != $current_month_key) {
//                $datas[] = [
//                    'idkey' => $current_month_key,
//                    'value' => $month_str[intval(Carbon::now()->format('m')) - 1] . ' ' . Carbon::now()->format('Y'),
//                    'cur' => intval(Carbon::now()->format('m')),
//                    'year' => intval(Carbon::now()->format('Y'))
//                ];
//            }
//        } else {
//            $current_month_key = Carbon::now()->format('Y-m');
//            $datas[] = [
//                'idkey' => $current_month_key,
//                'value' => $month_str[intval(Carbon::now()->format('m')) - 1] . ' ' . Carbon::now()->format('Y'),
//                'cur' => intval(Carbon::now()->format('m')),
//                'year' => intval(Carbon::now()->format('Y'))
//            ];
        
//        }

        $current = intval(Carbon::now()->format('m'));
        $cYear = intval(Carbon::now()->format('Y'));
        $transformer_desc = [];
        $transformer_asc = [];
        foreach ($months as $month) {
            $ex = explode('-', $month->idkey);
            $month->value = $month_str[intval($ex[1]) - 1] . ' ' . $ex[0];
            $month->cur = intval($ex[1]);
            $month->year = intval($ex[0]);
            $month->selected = ($current == explode('-', $month->idkey)[1] ? 'y' : 'n');
            if($current <= explode('-', $month->idkey)[1] && explode('-', $month->idkey)[0] == $cYear && $current != explode('-', $month->idkey)[1]){
                $transformer_desc[] = $month;
            }else{
                $transformer_asc[] = $month;
            }
        }

        $firstDataset = collect($transformer_desc)->sortBy('cur');
        $ddr = collect($transformer_asc)->whereNotIn('cur',$firstDataset->pluck('cur')->values()->all())->sortByDesc('year')->values()->all();
        $final = array_merge($firstDataset->values()->all(),$ddr);

//        $filterData = collect($datas)->unique('idkey')->values()->all();
//        $response['data'] = $filterData;
        $response['data'] = $final;

        return $response;
    }

    public function getActiveOfficer(Request $request){
        $admin_id = $request->input('admin_id');
        $nl_month = $request->input('nl_month') ?? null;
        $nl_year = $request->input('nl_year') ?? null;
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeave::whereIn('nl_status', [1, 2, 3, -1]);
                if($nl_month != null){
                    $query->whereMonth('nl_leave_start', strval($nl_month));
                }
                if($nl_year != null){
                    $query->whereYear('nl_leave_start', strval($nl_year));
                }
                $query->where(function ($query) use ($filters) {
                    if ($filters) {
                        foreach ($filters as $col => $val) {
                            $query->where($col, $val);
                        }
                    }
                })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'nl_note'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->orderBy('nl_leave_start', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $officer = [];
            $packer = [];
            foreach ($nl_data as $data) {
                $info = Officer::where('off_id', $data->off_id)->where('off_status',1)->get()->first();
                if($info){
                    $officer['full_name'] = $info->off_prefixname . " " . $info->off_firstname . " " . $info->off_lastname;
                    $officer['idkey'] = $info->off_id;
                    $packer[] = $officer;
                }

            }

            $response['data'] = collect($packer)->unique('full_name')->values()->all();
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertNoteLeave(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_id = $request->input('off_id');
        $nl_type_id = $request->input('nl_type_id');
        $nl_time_type = $request->input('nl_time_type');
        $nl_note = $request->input('nl_note');
        $nl_leave_start = $request->input('nl_leave_start');
        $nl_leave_end = $request->input('nl_leave_end');
        $nl_time_start = $request->input('nl_time_start');
        $nl_time_end = $request->input('nl_time_end');

        $response = FuncHelper::response();

        $params_required = array(
            'addmin_id' => $admin_id,
            'off_id' => $off_id,
            'nl_type_id' => $nl_type_id,
            'nl_time_type' => $nl_time_type,
            'nl_note' => $nl_note,
            'nl_leave_start' => $nl_leave_start,
            'nl_leave_end' => $nl_leave_end
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = NoteLeave::create([
                'off_id' => $off_id,
                'nl_type_id' => $nl_type_id,
                'nl_time_type' => $nl_time_type,
                'nl_note' => $nl_note,
                'nl_leave_start' => $nl_leave_start,
                'nl_leave_end' => $nl_leave_end,
                'nl_time_start' => $nl_time_start,
                'nl_time_end' => $nl_time_end,
                'nl_status' => 3,
                'nl_approve_by' => $admin_id
            ]);

            $nl_data = NoteLeave::where('nl_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }
    public function updateNoteLeave(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $nl_id = $request->input('nl_id');
        $off_id = $request->input('off_id');
        $nl_type_id = $request->input('nl_type_id');
        $nl_time_type = $request->input('nl_time_type');
        $nl_note = $request->input('nl_note');
        $nl_leave_start = $request->input('nl_leave_start');
        $nl_leave_end = $request->input('nl_leave_end');
        $nl_time_start = $request->input('nl_time_start');
        $nl_time_end = $request->input('nl_time_end');
        $nl_status = $request->input('nl_status');
        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'nl_id' => $nl_id,
            'off_id' => $off_id,
            'nl_type_id' => $nl_type_id,
            'nl_time_type' => $nl_time_type,
            'nl_note' => $nl_note,
            'nl_leave_start' => $nl_leave_start,
            'nl_leave_end' => $nl_leave_end,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            NoteLeave::where('nl_id', $nl_id)
                ->update([
                    'nl_type_id' => $nl_type_id,
                    'nl_time_type' => $nl_time_type,
                    'nl_note' => $nl_note,
                    'nl_leave_start' => $nl_leave_start,
                    'nl_leave_end' => $nl_leave_end,
                    'nl_time_start' => $nl_time_start,
                    'nl_time_end' => $nl_time_end,
                    'nl_status' => $nl_status,
                    'updated_at' => Carbon::now('GMT+7')
                ]);

            $nl_data = NoteLeave::where('nl_id', $nl_id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateNoteLeaveStatus(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $nl_id = $request->input('nl_id');
        $nl_status = $request->input('nl_status');
        $nl_reject_comment = $request->input('nl_reject_comment') ?? null;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'nl_id' => $nl_id,
            'nl_status' => $nl_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $status_txt = '';
            if ($nl_status == 2) {
                NoteLeave::where('nl_id', $nl_id)
                    ->update([
                        'nl_status' => $nl_status,
                        'nl_review_by' => $admin_id
                    ]);
                $status_txt = 'รอตรวจสอบ';
            } else if ($nl_status == 3) {
                NoteLeave::where('nl_id', $nl_id)
                    ->update([
                        'nl_status' => $nl_status,
                        'nl_approve_by' => $admin_id
                    ]);
                $status_txt = 'อนุมัติ';
            } else if ($nl_status == -1) {
                NoteLeave::where('nl_id', $nl_id)
                    ->update([
                        'nl_status' => $nl_status,
                        'nl_reject_comment' => $nl_reject_comment,
                        'nl_reject_by' => $admin_id
                    ]);
                $status_txt = 'ไม่อนุมัติ';
            } else if ($nl_status == 0) {
                NoteLeave::where('nl_id', $nl_id)
                    ->update([
                        'nl_status' => $nl_status
                    ]);
                $status_txt = 'ยกเลิกการยื่น';
            }

            $nl_data = NoteLeave::where('nl_id', $nl_id)->first();
            $off_data = Officer::where('off_id', $nl_data->off_id)->first();
            $id_format = str_pad($nl_id, 5, '0', STR_PAD_LEFT);

            // send email
            if (config('app.env') == 'production') {
                $subject = 'ใบลางานอัพเดทเป็น' . $status_txt . ' (NLID:' . $id_format . ')';
                $name = $off_data->off_firstname . ' ' . $off_data->off_lastname;
                $email = $off_data->off_email;
                try {
                    EmailHelper::updateNoteLeave($email, $subject, $name, $status_txt);
                } catch (\Exception $e) {
                }
            }

            // send slack notification
            if ($nl_reject_comment) {
                $slack_message = 'ใบลา <http://officer.applicationdd.com/noteleave|*NoteLeave #' . $id_format . '*> ';
                $slack_message .= 'อัพเดทสถานะเป็น *' . $status_txt . '* เพราะ ' . $nl_reject_comment;
            } else {
                $slack_message = 'ใบลา <http://officer.applicationdd.com/noteleave|*NoteLeave #' . $id_format . '*> ';
                $slack_message .= 'อัพเดทสถานะเป็น *' . $status_txt . '*';
            }
//            SlackHelper::sendNLDM($off_data->off_slack_webhook_token, $slack_message);
            SlackHelper::sendChannel('team_lead', $slack_message);

            $nl_data = NoteLeave::where('nl_id', $nl_id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function noteLeaveReport(Request $request){
        $admin_id = $request->input('admin_id');
        $nl_month = $request->input('nl_month') ?? null;
        $nl_year = $request->input('nl_year') ?? null;
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeave::whereIn('nl_status', [3]);
                if($nl_month != null){
                    $query->whereMonth('nl_leave_start', strval($nl_month));
                }
                if($nl_year != null){
                    $query->whereYear('nl_leave_start', strval($nl_year));
                }
                $query->where(function ($query) use ($filters) {
                    if ($filters) {
                        foreach ($filters as $col => $val) {
                            $query->where($col, $val);
                        }
                    }
                });

                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->orderBy('nl_leave_start', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $getNoteLeaveDimension = [];

            foreach ($nl_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
                $data->note_leave_type_data = NoteLeaveType::where('nl_type_id', $data->nl_type_id)->first();
                $getNoteLeaveDimension[] = NoteLeaveType::where('nl_type_id', $data->nl_type_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getNoteLeaveDimension(Request $request){
        $noteLeaveDimension = NoteLeaveType::query()->get()->unique('nl_type_name')->values()->all();

        $response['data'] = $noteLeaveDimension;
        return $response;
    }

}