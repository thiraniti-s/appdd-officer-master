<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\EmailHelper;
use App\Helpers\EncodeHelper;
use App\Helpers\FuncHelper;
use App\Http\Controllers\Controller;
use App\Models\Officer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfficerController extends Controller
{
    public function getOfficer(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = DB::table('tb_officer')
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'off_firstname',
                                'off_lastname',
                                'off_nickname',
                                'off_personal_id',
                                'off_tel',
                                'off_email',
                                'off_position'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_data
            );
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function addOfficer(Request $request)
    {
        $admin_id = $request->input('admin_id');

        $off_type_id = $request->input('off_type_id');
        $off_firstname = $request->input('off_firstname');
        $off_lastname = $request->input('off_lastname');
        $off_email = $request->input('off_email');
        $off_tel = $request->input('off_tel');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_type_id' => $off_type_id,
            'off_firstname' => $off_firstname,
            'off_lastname' => $off_lastname,
            'off_email' => $off_email,
            'off_tel' => $off_tel,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        $input = $request->except('admin_id');

        if ($res_param->success) {
            try {
                      
                $query = DB::table('tb_officer')->insert([
                    'off_code' => $input['off_code'],
                    'off_type_id' => $input['off_type_id'],
                    'off_prefixname' => $input['off_prefixname'],
                    'off_firstname' => $input['off_firstname'],
                    'off_lastname' => $input['off_lastname'],
                    'off_nickname' => $input['off_nickname'],
                    'off_personal_id' => $input['off_personal_id'],
                    'off_bdate' =>  $input['off_bdate']? $input['off_bdate']:null,
                    'off_tel' => $input['off_tel'],
                    'off_email' => $input['off_email'],
                    'off_start_working' => $input['off_start_working']? $input['off_start_working']:null,
                    'off_end_working' => $input['off_end_working']? $input['off_end_working']:null,
                    'off_position' => $input['off_position'],
                    'off_bank_acc_name'	=> $input['off_bank_acc_name'],
                    'off_bank_acc_number' => $input['off_bank_acc_number'],
                    'off_username'	=> $input['off_username'],
                    'off_password'	=> EncodeHelper::getSHA256(md5($input['off_password'])),
                    'off_github_user'=> $input['off_github_user'],
                    'off_slack_user'	=> $input['off_slack_user'],
                    'off_permission_level' => $input['off_permission_level']
                ]);
           
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = $query;
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateOfficer(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_id = $request->input('off_id');
        $off_type_id = $request->input('off_type_id');
        $off_firstname = $request->input('off_firstname');
        $off_lastname = $request->input('off_lastname');
        $off_email = $request->input('off_email');
        $off_tel = $request->input('off_tel');
        $off_personal_id = $request->input('off_personal_id');
        $off_username = $request->input('off_username');
        $off_start_working = $request->input('off_start_working');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_id' => $off_id,
            'off_type_id' => $off_type_id,
            'off_firstname' => $off_firstname,
            'off_lastname' => $off_lastname,
            'off_email' => $off_email,
            'off_tel' => $off_tel,
            'off_personal_id' => $off_personal_id,
            'off_username' => $off_username,
            'off_start_working' => $off_start_working
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        $input = $request->except('admin_id');

        if ($res_param->success) {
            try {
                      
                $query = Officer::where('off_id', $off_id)
                ->update([
                    'off_code' => $input['off_code'],
                    'off_type_id' => $input['off_type_id'],
                    'off_prefixname' => $input['off_prefixname'],
                    'off_firstname' => $input['off_firstname'],
                    'off_lastname' => $input['off_lastname'],
                    'off_nickname' => $input['off_nickname'],
                    'off_personal_id' => $input['off_personal_id'],
                    'off_bdate' =>  $input['off_bdate']? $input['off_bdate']:null,
                    'off_tel' => $input['off_tel'],
                    'off_email' => $input['off_email'],
                    'off_start_working' => $input['off_start_working']? $input['off_start_working']:null,
                    'off_end_working' => $input['off_end_working']? $input['off_end_working']:null,
                    'off_position' => $input['off_position'],
                    'off_bank_acc_name'	=> $input['off_bank_acc_name'],
                    'off_bank_acc_number' => $input['off_bank_acc_number'],
                    'off_github_user'=> $input['off_github_user'],
                    'off_slack_user'	=> $input['off_slack_user'],
                    'off_permission_level' => $input['off_status']== 0? 0:$input['off_permission_level'],
                    'off_status' => $input['off_status']
                ]);
           
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = $query;
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getOfficerDetail(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_id = $request->input('off_id');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_id' => $off_id
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        $input = $request->except('admin_id');

        if ($res_param->success) {
            try {
                      
                $query = Officer::where('off_id', $off_id)
                         ->first();

                $response['data'] = $query;
                return $response;
           
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = $query;
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function generateAccount(Request $request)
    {
        $off_id = $request->input('off_id');

        $response = FuncHelper::response();

        $params_required = array(
            'off_id' => $off_id
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $officer = Officer::where('off_id', $off_id)
                ->where('off_status', 1)
                ->first();

            if ($officer) {
                $password = $this->generatePassword(6);
                try {
                    Officer::where('off_id', $off_id)
                        ->update([
                            'off_password' => EncodeHelper::getSHA256(md5($password))
                        ]);

                    // send email
                    EmailHelper::sendGenerateAccount($officer->off_email, $officer->off_firstname . ' ' . $officer->off_lastname, $officer->off_username, $password);

                    $response['data'] = $officer;
                    return $response;

                } catch (QueryException $e) {
                    return FuncHelper::responseError($e->getMessage());
                } catch (\Exception $e) {
                    return FuncHelper::responseError($e->getMessage());
                }
            } else {
                return FuncHelper::responseError('Invalid officer id');
            }
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    // private functions

    private function generatePassword($length = 8)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz123456789';
        $charactersLength = strlen($characters);
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= $characters[rand(0, $charactersLength - 1)];
        }
        return $password;
    }

}