<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function getProject(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = Project::whereIn('pro_status', [1, 2, 3])
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'pro_name'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $pro_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('pro_id', 'desc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $pro_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertProject(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $pro_name = $request->input('pro_name');
        $pro_description = $request->input('pro_description');
        $pro_application = $request->input('pro_application');
        $pro_skill = $request->input('pro_skill');
        $pro_tools = $request->input('pro_tools');
        $pro_start = $request->input('pro_start');
        $pro_status = $request->input('pro_status');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'pro_name' => $pro_name,
            'pro_description' => $pro_description,
            'pro_application' => $pro_application,
            'pro_skill' => $pro_skill,
            'pro_tools' => $pro_tools,
            'pro_start' => $pro_start
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = Project::create([
                'pro_name' => $pro_name,
                'pro_description' => $pro_description,
                'pro_application' => $pro_application,
                'pro_skill' => $pro_skill,
                'pro_tools' => $pro_tools,
                'pro_start' => $pro_start,
                'pro_status' => $pro_status,
                'created_at' => Carbon::now('GMT+7'),
                'updated_at' => Carbon::now('GMT+7')
            ]);

            $pro_data = Project::where('pro_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($pro_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateProject(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $pro_id = $request->input('pro_id');
        $pro_name = $request->input('pro_name');
        $pro_description = $request->input('pro_description');
        $pro_application = $request->input('pro_application');
        $pro_skill = $request->input('pro_skill');
        $pro_tools = $request->input('pro_tools');
        $pro_start = $request->input('pro_start');
        $pro_status = $request->input('pro_status');
        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'pro_id' => $pro_id,
            'pro_name' => $pro_name,
            'pro_description' => $pro_description,
            'pro_application' => $pro_application,
            'pro_skill' => $pro_skill,
            'pro_tools' => $pro_tools,
            'pro_start' => $pro_start,
            'pro_status' => $pro_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            Project::where('pro_id', $pro_id)
                ->update([
                    'pro_name' => $pro_name,
                    'pro_description' => $pro_description,
                    'pro_application' => $pro_application,
                    'pro_skill' => $pro_skill,
                    'pro_tools' => $pro_tools,
                    'pro_start' => $pro_start,
                    'pro_status' => $pro_status,
                    'updated_at' => Carbon::now('GMT+7')
                ]);

            $pro_data = Project::where('pro_id', $pro_id)->first();
            $response['data'] = FuncHelper::responseData(array($pro_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}