<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\EmailHelper;
use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\Officer;
use App\Models\OverTime;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OverTimeController extends Controller
{
    public function getOverTime(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $ot_month = $request->input('ot_month') ?? Carbon::now('GMT+7')->format('m');
        $ot_year = $request->input('ot_year') ?? Carbon::now('GMT+7')->format('Y');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = OverTime::whereIn('ot_status', [1, 2, 3, -1])
                    ->whereMonth('ot_date', strval($ot_month))
                    ->whereYear('ot_date', strval($ot_year))
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'ot_note'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $ot_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_id', 'asc')
                    ->orderBy('ot_date', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            foreach ($ot_data as $data) {
                $data->officer_data = Officer::where('off_id', $data->off_id)->first();
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $ot_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function getOverTimeMonth()
    {
        $response = FuncHelper::response();

        $months = OverTime::select(DB::raw('DATE_FORMAT(ot_date, "%Y-%m") as idkey'))
            ->groupby('idkey')
            ->orderByDesc('idkey')
            ->limit(3)
            ->get();

        $month_str = [
            'มกราคม',
            'กุมภาพันธ์',
            'มีนาคม',
            'เมษายน',
            'พฤษภาคม',
            'มิถุนายน',
            'กรกฎาคม',
            'สิงหาคม',
            'กันยายน',
            'ตุลาคม',
            'พฤศจิกายน',
            'ธันวาคม'
        ];
        $datas = [];
        if (count($months) > 0) {
            $current_month_key = Carbon::now()->format('Y-m');
            if ($months[0]->idkey != $current_month_key) {
                $datas[] = [
                    'idkey' => $current_month_key,
                    'value' => $month_str[intval(Carbon::now()->format('m')) - 1] . ' ' . Carbon::now()->format('Y')
                ];
            }
        } else {
            $current_month_key = Carbon::now()->format('Y-m');
            $datas[] = [
                'idkey' => $current_month_key,
                'value' => $month_str[intval(Carbon::now()->format('m')) - 1] . ' ' . Carbon::now()->format('Y')
            ];
        }
        foreach ($months as $month) {
            $ex = explode('-', $month->idkey);
            $month->value = $month_str[intval($ex[1]) - 1] . ' ' . $ex[0];
            $datas[] = $month;
        }

        $response['data'] = $datas;

        return $response;
    }

    public function insertOverTime(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_id = $request->input('off_id');
        $ot_note = $request->input('ot_note');
        $ot_reference = $request->input('ot_reference');
        $ot_date = $request->input('ot_date');
        $ot_time_start = $request->input('ot_time_start');
        $ot_time_end = $request->input('ot_time_end');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_id' => $off_id,
            'ot_note' => $ot_note,
            'ot_reference' => $ot_reference,
            'ot_date' => $ot_date,
            'ot_time_start' => $ot_time_start,
            'ot_time_end' => $ot_time_end
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = OverTime::create([
                'off_id' => $off_id,
                'ot_note' => $ot_note,
                'ot_reference' => $ot_reference,
                'ot_date' => $ot_date,
                'ot_time_start' => $ot_time_start,
                'ot_time_end' => $ot_time_end,
                'ot_status' => 3,
                'ot_approve_by' => $admin_id
            ]);

            $ot_data = OverTime::where('ot_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($ot_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateOverTimeStatus(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $ot_id = $request->input('ot_id');
        $ot_status = $request->input('ot_status');
        $ot_reject_comment = $request->input('ot_reject_comment') ?? null;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'ot_id' => $ot_id,
            'ot_status' => $ot_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $status_txt = '';
            if ($ot_status == 2) {
                OverTime::where('ot_id', $ot_id)
                    ->update([
                        'ot_status' => $ot_status,
                        'ot_review_by' => $admin_id
                    ]);
                $status_txt = 'รอตรวจสอบ';
            } else if ($ot_status == 3) {
                OverTime::where('ot_id', $ot_id)
                    ->update([
                        'ot_status' => $ot_status,
                        'ot_approve_by' => $admin_id
                    ]);
                $status_txt = 'อนุมัติ';
            } else if ($ot_status == -1) {
                OverTime::where('ot_id', $ot_id)
                    ->update([
                        'ot_status' => $ot_status,
                        'ot_reject_comment' => $ot_reject_comment,
                        'ot_reject_by' => $admin_id
                    ]);
                $status_txt = 'ไม่อนุมัติ';
            } else if ($ot_status == 0) {
                OverTime::where('ot_id', $ot_id)
                    ->update([
                        'ot_status' => $ot_status
                    ]);
                $status_txt = 'ยกเลิกการยื่น';
            }

            // send email
            if (config('app.env') == 'production') {
                $ot_data = OverTime::where('ot_id', $ot_id)->first();
                $off_data = Officer::where('off_id', $ot_data->off_id)->first();
                $subject = 'ใบล่วงเวลาอัพเดทเป็น' . $status_txt . ' (OTID:' . str_pad($ot_id, 5, '0', STR_PAD_LEFT) . ')';
                $name = $off_data->off_firstname . ' ' . $off_data->off_lastname;
                $email = $off_data->off_email;
                try {
                    EmailHelper::updateOverTime($email, $subject, $name, $status_txt);
                } catch (\Exception $e) {
                }
            }

            $ot_data = OverTime::where('ot_id', $ot_id)->first();
            $response['data'] = FuncHelper::responseData(array($ot_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}