<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\OfficerType;
use Illuminate\Http\Request;

class OfficerTypeController extends Controller
{
    public function getOfficerType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = OfficerType::whereIn('off_type_status', [1, 0])
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'off_type_name'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $off_type_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('off_type_id', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $off_type_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertOfficerType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_type_name = $request->input('off_type_name');
        $off_type_status = $request->input('off_type_status') ?? 1;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_type_name' => $off_type_name,
            'off_type_status' => $off_type_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = OfficerType::create([
                'off_type_name' => $off_type_name,
                'off_type_status' => $off_type_status
            ]);

            $off_type_data = OfficerType::where('off_type_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($off_type_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateOfficerType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $off_type_id = $request->input('off_type_id');
        $off_type_name = $request->input('off_type_name');
        $off_type_status = $request->input('off_type_status');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'off_type_id' => $off_type_id,
            'off_type_name' => $off_type_name,
            'off_type_status' => $off_type_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            OfficerType::where('off_type_id', $off_type_id)
                ->update([
                    'off_type_name' => $off_type_name,
                    'off_type_status' => $off_type_status
                ]);

            $off_type_data = OfficerType::where('off_type_id', $off_type_id)->first();
            $response['data'] = FuncHelper::responseData(array($off_type_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}