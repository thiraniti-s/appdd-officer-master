<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\NoteLeave;
use App\Models\Officer;
use App\Models\OverTime;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getSummary(Request $request)
    {
        $admin_id = $request->input('admin_id');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $data = array();

            $data['count_officers'] = Officer::where('off_status', 1)->count();
            $data['count_request_noteleaves'] = NoteLeave::whereIn('nl_status', [1, 2])->count();
            $data['count_approve_noteleaves'] = NoteLeave::where('nl_status', 3)->count();
            $data['count_request_overtimes'] = OverTime::whereIn('ot_status', [1, 2])->count();
            $data['count_approve_overtimes'] = OverTime::where('ot_status', 3)->count();

            $response['data'] = FuncHelper::responseData($data);
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}