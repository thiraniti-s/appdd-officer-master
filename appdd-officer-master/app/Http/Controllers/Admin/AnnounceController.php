<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\Announce;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AnnounceController extends Controller
{
    public function getAnnounce(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id2' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = Announce::whereIn('ann_status', [1, 0])
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'ann_text'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $ann_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('ann_id', 'desc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError('555' . $e->getMessage());
            }

            foreach ($ann_data as $data) {
                $data->officer;
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $ann_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertAnnounce(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $ann_text = $request->input('ann_text');
        $ann_date = $request->input('ann_date');
        $ann_status = $request->input('ann_status');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'ann_text' => $ann_text,
            'ann_date' => $ann_date,
            'ann_status' => $ann_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = Announce::create([
                'off_id' => $admin_id,
                'ann_text' => $ann_text,
                'ann_date' => $ann_date,
                'ann_status' => $ann_status,
                'created_at' => Carbon::now('GMT+7'),
                'updated_at' => Carbon::now('GMT+7')
            ]);

            $ann_data = Announce::where('ann_id', $model->id)->first();
            if ($ann_data) {
                $ann_data->officer;
            }
            $response['data'] = FuncHelper::responseData(array($ann_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateAnnounce(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $ann_id = $request->input('ann_id');
        $ann_text = $request->input('ann_text');
        $ann_date = $request->input('ann_date');
        $ann_status = $request->input('ann_status');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'ann_id' => $ann_id,
            'ann_text' => $ann_text,
            'ann_date' => $ann_date,
            'ann_status' => $ann_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            Announce::where('ann_id', $ann_id)
                ->update([
                    'ann_text' => $ann_text,
                    'ann_date' => $ann_date,
                    'ann_status' => $ann_status,
                    'updated_at' => Carbon::now('GMT+7')
                ]);

            $ann_data = Announce::where('ann_id', $ann_id)->first();
            if ($ann_data) {
                $ann_data->officer;
            }
            $response['data'] = FuncHelper::responseData(array($ann_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}