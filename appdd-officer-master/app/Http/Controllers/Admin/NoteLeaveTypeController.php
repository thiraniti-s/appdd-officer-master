<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\FuncHelper;
use App\Models\NoteLeaveType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class NoteLeaveTypeController extends Controller
{
    public function getNoteLeaveType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $search = $request->input('search') ?? null;
        $filters = $request->input('filters') ?? null;
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 50;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            try {
                $query = NoteLeaveType::whereIn('nl_type_status', [1, 0])
                    ->where(function ($query) use ($filters) {
                        if ($filters) {
                            foreach ($filters as $col => $val) {
                                $query->where($col, $val);
                            }
                        }
                    })
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $column_to_search = [
                                'nl_type_name'
                            ];
                            foreach ($column_to_search as $column) {
                                $query->orWhere($column, 'like', "%{$search}%");
                            }
                        }
                    });

                $count_all_data = $query->count();
                $nl_type_data = $query->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->orderBy('nl_type_id', 'asc')
                    ->get();
            } catch (QueryException $e) {
                return FuncHelper::responseError($e->getMessage());
            }

            $response['data'] = FuncHelper::responseResult(
                $search, $filters, $count_all_data, $page, $limit, $nl_type_data
            );

            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function insertNoteLeaveType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $nl_type_name = $request->input('nl_type_name');
        $nl_type_status = $request->input('nl_type_status') ?? 1;

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'nl_type_name' => $nl_type_name
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            $model = NoteLeaveType::create([
                'nl_type_name' => $nl_type_name,
                'nl_type_status' => $nl_type_status
            ]);

            $nl_type_data = NoteLeaveType::where('nl_type_id', $model->id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_type_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

    public function updateNoteLeaveType(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $nl_type_id = $request->input('nl_type_id');
        $nl_type_name = $request->input('nl_type_name');
        $nl_type_status = $request->input('nl_type_status');

        $response = FuncHelper::response();

        $params_required = array(
            'admin_id' => $admin_id,
            'nl_type_id' => $nl_type_id,
            'nl_type_name' => $nl_type_name,
            'nl_type_status' => $nl_type_status
        );
        $res_param = FuncHelper::checkRequiredParams($params_required);

        if ($res_param->success) {
            NoteLeaveType::where('nl_type_id', $nl_type_id)
                ->update([
                    'nl_type_name' => $nl_type_name,
                    'nl_type_status' => $nl_type_status
                ]);

            $nl_type_data = NoteLeaveType::where('nl_type_id', $nl_type_id)->first();
            $response['data'] = FuncHelper::responseData(array($nl_type_data));
            return $response;
        } else {
            return FuncHelper::responseError($res_param->message);
        }
    }

}