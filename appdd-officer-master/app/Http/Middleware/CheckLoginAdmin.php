<?php

namespace App\Http\Middleware;

use Closure;

class CheckLoginAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!session()->has('login')) {
            return redirect('/login');
        }
        if (session()->get('login')->off_permission_level == 0) {
            return redirect('/dashboard');
        }

        return $next($request);
    }
}
