<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficerType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_officer_type';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['off_type_name', 'off_type_status'];

}
