<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_officer';

    protected $guarded = [];

    protected $hidden = ['off_password'];

    protected $appends = [];

    public static function getRules($id = null)
    {
        if ($id) {
            $rules = [
                'off_code' => 'nullable|max:25',
                'off_type_id' => 'nullable|numeric',
                'off_prefixname' => 'required|max:25',
                'off_firstname' => 'required|max:100',
                'off_lastname' => 'required|max:100',
                'off_nickname' => 'nullable|max:50',
                'off_personal_id' => 'nullable|max:25',
                'off_bdate' =>  'date_format:Y-m-d|nullable',
                'off_tel' => 'required|max:25',
                'off_email' => 'nullable|max:100',
                'off_start_working' => 'date_format:Y-m-d|nullable',
                'off_end_working' => 'date_format:Y-m-d|nullable',
                'off_position' => 'nullable|max:50',
                'off_bank_acc_name'	=> 'nullable|max:50',
                'off_bank_acc_number' => 'nullable|max:50',
                'off_username'	=> 'nullable|max:50',
                'off_password'	=> 'nullable|max:100',
                'off_github_user'=> 'nullable|max:255',
                'off_slack_user'	=> 'nullable|max:255',
                'off_permission_level' => 'required|in:0,1'
            ];
        } else {
            $rules = [
                'off_code' => 'nullable|max:25',
                'off_type_id' => 'nullable|numeric',
                'off_prefixname' => 'required|max:25',
                'off_firstname' => 'required|max:100',
                'off_lastname' => 'required|max:100',
                'off_nickname' => 'nullable|max:50',
                'off_personal_id' => 'nullable|max:25',
                'off_bdate' =>  'date_format:Y-m-d|nullable',
                'off_tel' => 'required|max:25',
                'off_email' => 'nullable|max:100',
                'off_start_working' => 'date_format:Y-m-d|nullable',
                'off_end_working' => 'date_format:Y-m-d|nullable',
                'off_position' => 'nullable|max:50',
                'off_bank_acc_name'	=> 'nullable|max:50',
                'off_bank_acc_number' => 'nullable|max:50',
                'off_username'	=> 'nullable|max:50',
                'off_password'	=> 'nullable|max:100',
                'off_github_user'=> 'nullable|max:255',
                'off_slack_user'	=> 'nullable|max:255',
                'off_permission_level' => 'required|in:0,1'
            ];
        }
        return $rules;
    }

    /**
     * Get the officer type associated with the officer.
     */
    public function off_type()
    {
        return $this->hasOne(OfficerType::class, 'off_type_id', 'off_type_id');
    }

}
