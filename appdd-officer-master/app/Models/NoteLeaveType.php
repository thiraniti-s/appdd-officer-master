<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteLeaveType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_note_leave_type';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nl_type_name', 'nl_type_status'];

}
