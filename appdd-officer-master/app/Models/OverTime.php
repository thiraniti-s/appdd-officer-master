<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OverTime extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_over_time';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'off_id',
        'ot_note',
        'ot_reference',
        'ot_date',
        'ot_time_start',
        'ot_time_end',
        'ot_status',
        'ot_approve_by'
    ];

}
