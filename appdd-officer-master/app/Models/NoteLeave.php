<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteLeave extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_note_leave';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'off_id',
        'nl_type_id',
        'nl_time_type',
        'nl_note',
        'nl_leave_start',
        'nl_leave_end',
        'nl_time_start',
        'nl_time_end',
        'nl_status',
        'nl_approve_by'
    ];

}
