<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_project';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pro_name',
        'pro_description',
        'pro_application',
        'pro_skill',
        'pro_tools',
        'pro_start',
        'pro_status',
        'created_at',
        'updated_at',
    ];

}
