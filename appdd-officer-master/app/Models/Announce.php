<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announce extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_announce';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'off_id',
        'ann_text',
        'ann_date',
        'ann_status'
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    /**
     * Get the officer associated with the announce.
     */
    public function officer()
    {
        return $this->hasOne(Officer::class, 'off_id', 'off_id');
    }

}
