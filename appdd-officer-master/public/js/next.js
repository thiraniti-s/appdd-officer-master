function _log(message) {
  console.log(message);
}

function _nextRedirect(url) {
  window.location.href = url;
}

function _nextReload() {
  window.location.reload();
}

function _nextRedirectBack() {
  window.history.back();
}

function _nextOpenLoading() {
  $('#modal_loading').modal({keyboard: false, backdrop: 'static'});
}

function _nextCloseLoading() {
  $('#modal_loading').modal('hide');
}

function _checkEmailFormat (email) {
  let emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailPattern.test(email);
}

function _momentDate(datetime, type = 'short') {
  let year = parseInt(moment(datetime).format('YYYY')) + 543;
  let date_month;
  if (type == 'short') {
    date_month = moment(datetime).format('D MMM');
  }
  else {
    date_month = moment(datetime).format('D MMMM');
  }
  return date_month + ' ' + year;
}

function _momentDateSuperShort(datetime) {
  let date_month;
  date_month = moment(datetime).format('D');
  return date_month;
}

function _momentTime(time) {
  let datetime = moment().format('YYYY-MM-DD ' + time);
  return moment(datetime).format('HH:mm [น.]');
}

function _momentDateTime(datetime, type = 'short') {
  let year = parseInt(moment(datetime).format('YYYY')) + 543;
  let date_month;
  if (type == 'short') {
    date_month = moment(datetime).format('D MMM');
  }
  else {
    date_month = moment(datetime).format('D MMMM');
  }
  let time = moment(datetime).format('HH:mm [น.]');

  return date_month + ' ' + year + ' ' + time;
}

function _momentDiffDateWork(date_start, date_end) {
  let datetime_start = moment().format(date_start + ' HH:mm:ss');
  let datetime_end = moment().format(date_end + ' HH:mm:ss');
  let diff = moment(datetime_start).diff(moment(datetime_end));
  let duration = moment.duration(diff);
  return parseInt(duration.get("days"));
}

function _momentDiffTimeWork(time_start, time_end) {
  let datetime_start = moment().format('YYYY-MM-DD ' + time_start);
  let datetime_end = moment().format('YYYY-MM-DD ' + time_end);
  let diff = moment(datetime_end).diff(moment(datetime_start));
  let duration = moment.duration(diff);
  var hours = parseInt(duration.get("hours"));
  if (parseInt(moment(datetime_start).format('H')) < 12 && parseInt(moment(datetime_end).format('H')) >= 13) {
    hours = hours - 1; // remove hour lanch time (12.00-13.00)
  }
  return hours;
}

function _nextZeroPadLeft(num, width, sign) {
  num = num + '';
  width = width || 6;
  sign = sign || '0';
  return num.length >= width ? num : new Array(width - num.length + 1).join(sign) + num;
}

function _nextIsValid(data, value, valid_msg) {
  if (comp == data) {
    return true;
  }
  else {
    swal({
      text: valid_msg,
      icon: 'warning'
    });
    return false;
  }
}

function _nextIsNotValid(data, value, valid_msg) {
  if (data != value) {
    return true;
  }
  else {
    swal({
      text: valid_msg,
      icon: 'warning'
    });
    return false;
  }
}

function _nextAlert(msg, type, title = null) {
  if (type === 'suc') {
    let title_text = title != null ? title : 'Success';
    swal({
      title: title_text,
      text: msg,
      icon: 'success',
      closeOnClickOutside: false
    });
  }
  else if (type === 'war') {
    let title_text = title != null ? title : 'Warning';
    swal({
      title: title_text,
      text: msg,
      icon: 'warning',
      closeOnClickOutside: false
    });
  }
  else if (type === 'err') {
    let title_text = title != null ? title : 'Error';
    swal({
      title: title_text,
      text: msg,
      icon: 'error',
      closeOnClickOutside: false
    });
  }
  else if (type === 'inf') {
    let title_text = title != null ? title : 'Info';
    swal({
      title: title_text,
      text: msg,
      icon: 'info',
      closeOnClickOutside: false
    });
  }
  else {
    swal({
      text: msg
    });
  }
}